import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  socket: any;

  constructor() {
    this.socket = io.connect(environment.apiSocket, {
      query: 'token=' + localStorage.getItem('token')
    });
  }

  getData(room) {
    return Observable.create(observer => {
      this.socket.on(room, res => {
        observer.next(res);
      });
    });
  }

}
