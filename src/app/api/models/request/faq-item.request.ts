import { FaqItemTransalationsRequest } from './faq-item.translation.request';

export class FaqItemRequest {
    path: string;
    translations: FaqItemTransalationsRequest[];
    status: boolean;
}