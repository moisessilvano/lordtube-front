export class FaqItemTransalationsRequest {
    language: string;
    title: string;
    tags: string;
    content: string;
}