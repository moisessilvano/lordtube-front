export class ChatMessageRequest {
    roomId: string;
    message: string;
}