export class GameRequest {
    title: string;
    link: string;
    image: string;
    status: boolean;
}