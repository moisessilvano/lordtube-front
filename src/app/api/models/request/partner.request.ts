export class PartnerRequest {
    title: string;
    banner: string;
    status: boolean;
}