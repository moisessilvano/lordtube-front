import { PageTransalationRequest } from './page.translation.request';

export class PageRequest {
    path: string;
    translations: PageTransalationRequest[];
    status: boolean;
    isPublic: boolean;
}