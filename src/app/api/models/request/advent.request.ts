export class AdventRequest {
    title: string;
    link: string;
    image: string;
    htmlCode: string;
    type: string;
    status: boolean;
}