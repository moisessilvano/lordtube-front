export class ReportRequest {
    item: string;
    type: string;
    message: string;
}