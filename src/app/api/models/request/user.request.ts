export class UserRequest {
    _id?: string;
    username?: string;
    password?: string;
    email?: string;
    picture?: string;
    cover?: string;
    name?: string;
    description?: string;
    createAt?: string;
    updateAt?: string;
    coins?: string;
    level?: string;
    isOn?: boolean;
    status?: boolean;
    recaptcha?: string;
}
