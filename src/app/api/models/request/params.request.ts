export class ParamsRequest {
    offset: number;
    limit: number;
}