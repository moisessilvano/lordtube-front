export class PageTransalationRequest {
    language: string;
    title: string;
    tags: string;
    content: string;
}