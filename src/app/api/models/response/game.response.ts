export class GameResponse {
    _id: string;
    title: string;
    link: string;
    image: string;
    status: boolean;
    createAt: string;
    views: number;
}