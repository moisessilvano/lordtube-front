export class AdventResponse {
    _id: string;
    title: string;
    link: string;
    image: string;
    htmlCode: string;
    type: number;
    status: boolean;
    createAt: string;
    updateAt: string;
}