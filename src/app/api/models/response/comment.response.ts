import { UserResponse } from './user.response';

export class CommentResponse {
    _id: string;
    content: string;
    createAt: string;
    post: string;
    user: UserResponse;
}
