export class FaqItemTranslationsResponse {
    language: string;
    title: string;
    tags: string;
    content: string;
}