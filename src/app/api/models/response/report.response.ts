import { UserResponse } from "./user.response";

export class ReportResponse {
  _id: string;
  user: UserResponse;
  item: string;
  type: string;
  message: string;
  createAt: string;
  resolved: {
    user: UserResponse;
    date: string;
    message: string;
    statusMessage: string;
  };
}
