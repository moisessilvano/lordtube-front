import { UserResponse } from './user.response';

export class PostResponse {
    _id: string;
    content: string;
    image: string;
    createAt: string;
    user: UserResponse;
    urlData: {
        url: string,
        title: string,
        description: string,
        image: string,
        keywords: string,
        videoId: string,
        type: string
    }
}
