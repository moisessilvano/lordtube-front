import { UserResponse } from './user.response';

export class UserPopularResponse {
    _id: string;
    count: number;
    user: UserResponse;
}