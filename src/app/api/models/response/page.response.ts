import { PageTranslationResponse } from './page-translations.response';

export class PageResponse {
    _id: string;
    path: string;
    translations: PageTranslationResponse[];
    status: boolean;
    isPublic: boolean;
    createAt: string;
    updateAt: string;
}