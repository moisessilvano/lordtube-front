import { FaqItemTranslationsResponse } from './faq-item-translations.response';

export class FaqItemResponse {
    _id: string;
    path: string;
    translations: FaqItemTranslationsResponse[];
    status: boolean;
    createAt: string;
    updateAt: string;
}