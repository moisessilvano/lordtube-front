import { UserResponse } from './user.response';

export class UserNotificationResponse {
    isSeen: boolean;
    message: string;
    updateAt: string;
    type: string;
    userSender: UserResponse;
    item: string;
}