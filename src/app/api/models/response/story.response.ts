import { UserResponse } from './user.response';

export class StoryResponse {
    _id: string;
    user: UserResponse;
    image: string;
    text: number;
    createAt: string;
}