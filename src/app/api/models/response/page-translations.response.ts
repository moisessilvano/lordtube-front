export class PageTranslationResponse {
    language: string;
    title: string;
    tags: string;
    content: string;
}