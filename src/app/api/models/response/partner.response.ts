export class PartnerResponse {
    _id: string;
    title: string;
    banner: string;
    link: string;
    createAt: string;
    updateAt: string;
    status: boolean;
}