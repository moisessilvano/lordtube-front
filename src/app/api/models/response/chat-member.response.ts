import { UserResponse } from './user.response';

export class ChatMemberResponse {
    _id: string;
    level: number;
    user: UserResponse;
}