import { UserResponse } from './user.response';

export class SharingResponse {
    url: string;
    image: string;
    type: string;
    user: string;
}
