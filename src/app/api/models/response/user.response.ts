export class UserResponse {
    _id: string;
    username: string;
    email: string;
    picture: string;
    cover: string;
    name: string;
    description: string;
    createAt: string;
    updateAt: string;
    coins: string;
    level: number;
    isOn: boolean;
    status: boolean;
    shareRef: string;
}
