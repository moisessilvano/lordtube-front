import { ChatMemberResponse } from './chat-member.response';

export class ChatRoomResponse {
    _id: string;
    title: string;
    description: string;
    updateAt: string;
    type: string;
    members: ChatMemberResponse[];
    lastMessage: {
        user: string;
        message: string;
        updateAt: string;
    };
    qntNotSeen: number;
}