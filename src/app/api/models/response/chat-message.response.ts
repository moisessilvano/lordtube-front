import { UserResponse } from 'src/app/api/models/response/user.response';

export class ChatMessageResponse {
    _id: string;
    user: string;
    messages: string[];
    createAt: string;
}