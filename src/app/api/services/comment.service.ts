import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CommentResponse } from '../models/response/comment.response';
import { CommentRequest } from '../models/request/comment.request';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private apiUrl = environment.apiUrl + 'api/comments';

  constructor(
    private http: HttpClient
  ) { }

  createComment(comment: CommentRequest) {
    return this.http.post<any>(`${this.apiUrl}`, comment);
  }

  getCommentsByPost(postId: string, params): Observable<CommentResponse[]> {
    return this.http.get<any>(`${this.apiUrl}/getByPost/${postId}`, { params: params });
  }

  getCommentsByUser(userId: string, params): Observable<CommentResponse[]> {
    return this.http.get<any>(`${this.apiUrl}/getByUser/${userId}`, { params: params });
  }
}
