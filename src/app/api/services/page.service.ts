import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageRequest } from '../models/request/page.request';
import { FaqItemResponse } from '../models/response/faq-item.response';
import { PageResponse } from '../models/response/page.response';

@Injectable({
    providedIn: 'root'
})
export class PageService {
    private apiUrl = environment.apiUrl + 'api/pages';

    constructor(
        private http: HttpClient
    ) { }

    create(page: PageRequest) {
        return this.http.post<any>(`${this.apiUrl}`, page);
    }

    update(id: string, page: PageRequest) {
        return this.http.put<any>(`${this.apiUrl}/${id}`, page);
    }

    getAll(params): Observable<PageResponse[]> {
        return this.http.get<any>(`${this.apiUrl}`, { params });
    }

    getById(id: string): Observable<PageResponse> {
        return this.http.get<any>(`${this.apiUrl}/${id}`);
    }

    getByUrl(url: string): Observable<PageResponse> {
        return this.http.get<any>(`${environment.apiUrl}pages/getByUrl/${url}`);
    }

}
