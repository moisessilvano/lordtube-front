import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageRequest } from '../models/request/page.request';
import { FaqItemResponse } from '../models/response/faq-item.response';
import { PageResponse } from '../models/response/page.response';
import { PartnerRequest } from '../models/request/partner.request';
import { PartnerResponse } from '../models/response/partner.response';
import { AdventRequest } from '../models/request/advent.request';
import { AdventResponse } from '../models/response/advent.response';
import { GameRequest } from '../models/request/game.request';
import { GameResponse } from '../models/response/game.response';

@Injectable({
    providedIn: 'root'
})
export class GameService {
    private apiUrl = environment.apiUrl + 'api/games';
    private apiUrlPublic = environment.apiUrl + 'games';

    constructor(
        private http: HttpClient
    ) { }

    create(game: GameRequest) {
        return this.http.post<any>(`${this.apiUrl}`, game);
    }

    update(id: string, game: GameRequest) {
        return this.http.put<any>(`${this.apiUrl}/${id}`, game);
    }

    updateViews(id: string) {
        return this.http.put<any>(`${this.apiUrlPublic}/updateViews/${id}`, {});
    }

    getAll(params): Observable<GameResponse[]> {
        return this.http.get<any>(`${this.apiUrlPublic}`, { params });
    }

    getById(id: string): Observable<GameResponse> {
        return this.http.get<any>(`${this.apiUrlPublic}/${id}`);
    }

}
