import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { UserResponse } from '../models/response/user.response';
import { UserRequest } from '../models/request/user.request';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private apiUrl = environment.apiUrl + 'api/users';

    constructor(
        private http: HttpClient
    ) { }

    getUser(userId: string): Observable<UserResponse> {
        return this.http.get<any>(`${this.apiUrl}/${userId}`);
    }

    getCurrentUser(): Observable<UserResponse> {
        return this.http.get<any>(`${this.apiUrl}/getCurrentUser`);
    }

    getByUsername(username: string): Observable<UserResponse> {
        return this.http.get<any>(`${this.apiUrl}/getByUsername/${username}`);
    }

    getById(userId: string): Observable<UserResponse> {
        return this.http.get<any>(`${this.apiUrl}/${userId}`);
    }

    getUsersOn(userId: string): Observable<UserResponse[]> {
        return this.http.get<any>(`${this.apiUrl}/getUsersOn`);
    }

    getTeam(): Observable<UserResponse[]> {
        return this.http.get<any>(`${this.apiUrl}/getTeam`);
    }

    getLastUsers(params): Observable<UserResponse[]> {
        return this.http.get<any>(`${this.apiUrl}/getLastUsers`, { params });
    }

    updateData(user: UserRequest) {
        return this.http.put<any>(`${this.apiUrl}`, user);
    }

    updatePassword(data: any) {
        return this.http.put<any>(`${this.apiUrl}/updatePassword`, data);
    }

    updatePicture(file: string) {
        return this.http.put<any>(`${this.apiUrl}/updatePicture`, { file: file });
    }

    updateCover(file: string) {
        return this.http.put<any>(`${this.apiUrl}/updateCover`, { file: file });
    }

}
