import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageRequest } from '../models/request/page.request';
import { FaqItemResponse } from '../models/response/faq-item.response';
import { PageResponse } from '../models/response/page.response';
import { PartnerRequest } from '../models/request/partner.request';
import { PartnerResponse } from '../models/response/partner.response';
import { AdventRequest } from '../models/request/advent.request';
import { AdventResponse } from '../models/response/advent.response';
import { UserNotificationResponse } from '../models/response/user-notification.response';

@Injectable({
    providedIn: 'root'
})
export class UserNotificationService {
    private apiUrl = environment.apiUrl + 'api/usernotifications';

    constructor(
        private http: HttpClient
    ) { }

    getByUser(): Observable<UserNotificationResponse[]> {
        return this.http.get<any>(`${this.apiUrl}/getByUser`);
    }

    updateSeen() {
        return this.http.put<any>(`${this.apiUrl}/updateSeen`, {});
    }

}
