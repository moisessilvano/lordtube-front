import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserRequest } from '../models/request/user.request';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl = environment.apiUrl + 'oauth';

  userIsLogged: boolean;

  constructor(
    private http: HttpClient
  ) { }

  checkAuth() {
    return (this.userIsLogged === true || localStorage.getItem('token') !== null);
  }

  auth(auth: UserRequest) {
    return this.http.post<any>(`${this.apiUrl}/token`, auth)
      .pipe(map(res => {
        if (res.token) {
          this.userIsLogged = true;
          localStorage.setItem('token', res.token);
        } else {
          this.userIsLogged = false;
        }

        return res;
      }));
  }

  logoutAuth() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    location.href = '/login';
    return true;
  }

  recoverPassword(auth: UserRequest) {
    return this.http.post<any>(`${this.apiUrl}/forgetPassword`, auth);
  }

  registerUser(auth: UserRequest) {
    return this.http.post<any>(`${this.apiUrl}/register`, auth);
  }

}
