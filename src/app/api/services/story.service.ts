import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageRequest } from '../models/request/page.request';
import { FaqItemResponse } from '../models/response/faq-item.response';
import { PageResponse } from '../models/response/page.response';
import { PartnerRequest } from '../models/request/partner.request';
import { PartnerResponse } from '../models/response/partner.response';
import { AdventRequest } from '../models/request/advent.request';
import { AdventResponse } from '../models/response/advent.response';
import { StoryRequest } from '../models/request/story.request';
import { StoryResponse } from '../models/response/story.response';

@Injectable({
    providedIn: 'root'
})
export class StoryService {
    private apiUrl = environment.apiUrl + 'api/stories';

    constructor(
        private http: HttpClient
    ) { }

    create(story: StoryRequest) {
        return this.http.post<any>(`${this.apiUrl}`, story);
    }

    delete(id: string) {
        return this.http.delete<any>(`${this.apiUrl}/${id}`);
    }

    getLast24Hours(params): Observable<StoryResponse[]> {
        return this.http.get<any>(`${this.apiUrl}/getLast24Hours`, { params });
    }

}
