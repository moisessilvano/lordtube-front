import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageRequest } from '../models/request/page.request';
import { FaqItemResponse } from '../models/response/faq-item.response';
import { PageResponse } from '../models/response/page.response';
import { PartnerRequest } from '../models/request/partner.request';
import { PartnerResponse } from '../models/response/partner.response';

@Injectable({
    providedIn: 'root'
})
export class PartnerService {
    private apiUrl = environment.apiUrl + 'api/partners';

    constructor(
        private http: HttpClient
    ) { }

    create(partner: PartnerRequest) {
        return this.http.post<any>(`${this.apiUrl}`, partner);
    }

    update(id: string, partner: PartnerRequest) {
        return this.http.put<any>(`${this.apiUrl}/${id}`, partner);
    }

    getAll(params): Observable<PartnerResponse[]> {
        return this.http.get<any>(`${this.apiUrl}`, { params });
    }

    getById(id: string): Observable<PartnerResponse> {
        return this.http.get<any>(`${this.apiUrl}/${id}`);
    }

}
