import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ChatMessageResponse } from '../models/response/chat-message.response';
import { ChatMessageRequest } from '../models/request/chat-message.request';

@Injectable({
    providedIn: 'root'
})
export class ChatMessageService {
    private apiUrl = environment.apiUrl + 'api/chatmessages';

    constructor(
        private http: HttpClient
    ) { }

    send(message: ChatMessageRequest) {
        return this.http.post<any>(`${this.apiUrl}/send`, message);
    }

    getByRoom(roomId: string, params): Observable<ChatMessageResponse[]> {
        return this.http.get<any>(`${this.apiUrl}/getByRoom/${roomId}`, { params });
    }

    resetSeenRoomByUser(roomId: string) {
        return this.http.put<any>(`${this.apiUrl}/resetSeenRoomByUser/${roomId}`, {});
    }

}
