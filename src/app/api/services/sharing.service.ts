import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharingRequest } from '../models/request/sharing.request';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { SharingResponse } from '../models/response/sharing.response';

@Injectable({
  providedIn: 'root'
})
export class SharingService {
  private apiUrl = environment.apiUrl + 'api/sharing';

  constructor(private http: HttpClient) { }

  send(sharingRequest: SharingRequest) {
    return this.http.post<any>(`${this.apiUrl}/sendItem`, sharingRequest);
  }

  getAll(params: any): Observable<SharingResponse[]> {
    return this.http.get<any>(`${this.apiUrl}`, { params: params });
  }
}
