import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ReportRequest } from '../models/request/report.request';
import { ReportResponse } from '../models/response/report.response';
import { FaqItemRequest } from '../models/request/faq-item.request';
import { FaqItemResponse } from '../models/response/faq-item.response';

@Injectable({
    providedIn: 'root'
})
export class FaqItemService {
    private apiUrl = environment.apiUrl + 'api/faqitems';

    constructor(
        private http: HttpClient
    ) { }

    create(faqItem: FaqItemRequest) {
        return this.http.post<any>(`${this.apiUrl}`, faqItem);
    }

    update(id: string, faqItem: FaqItemRequest) {
        return this.http.put<any>(`${this.apiUrl}/${id}`, faqItem);
    }

    getAll(params): Observable<FaqItemResponse[]> {
        return this.http.get<any>(`${this.apiUrl}`, { params });
    }

    getById(id: string): Observable<FaqItemResponse> {
        return this.http.get<any>(`${this.apiUrl}/${id}`);
    }

    getByPath(path: string): Observable<FaqItemResponse[]> {
        return this.http.get<any>(`${this.apiUrl}/getByPath/${path}`);
    }

}
