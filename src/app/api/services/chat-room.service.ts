import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ChatMessageRequest } from '../models/request/chat-message.request';
import { ChatRoomResponse } from '../models/response/chat-room.response';

@Injectable({
    providedIn: 'root'
})
export class ChatRoomService {
    private apiUrl = environment.apiUrl + 'api/chatrooms';

    constructor(
        private http: HttpClient
    ) { }

    getRoomsByUser(params): Observable<ChatRoomResponse[]> {
        return this.http.get<any>(`${this.apiUrl}/getRoomsByUser`, { params });
    }

    getRoomById(id): Observable<ChatRoomResponse> {
        return this.http.get<any>(`${this.apiUrl}/${id}`);
    }

    getRoomBeetwenUsers(userId: string): Observable<ChatRoomResponse> {
        return this.http.get<any>(`${this.apiUrl}/getRoomBeetwenUsers/${userId}`);
    }

}
