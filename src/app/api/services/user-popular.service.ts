import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageRequest } from '../models/request/page.request';
import { FaqItemResponse } from '../models/response/faq-item.response';
import { PageResponse } from '../models/response/page.response';
import { PartnerRequest } from '../models/request/partner.request';
import { PartnerResponse } from '../models/response/partner.response';
import { AdventRequest } from '../models/request/advent.request';
import { AdventResponse } from '../models/response/advent.response';
import { StoryRequest } from '../models/request/story.request';
import { StoryResponse } from '../models/response/story.response';
import { UserPopularResponse } from '../models/response/user-popular.response';
import { UserResponse } from '../models/response/user.response';

@Injectable({
    providedIn: 'root'
})
export class UserPopularService {
    private apiUrl = environment.apiUrl + 'api/userpopular';
    private apiUrlPublic = environment.apiUrl + 'userpopular';

    constructor(
        private http: HttpClient
    ) { }

    addRef(shareRef: string) {
        return this.http.post<any>(`${this.apiUrlPublic}/addRef`, { shareRef });
    }

    getShareRef(): Observable<UserResponse> {
        return this.http.get<any>(`${this.apiUrl}/getShareRef`);
    }

    getRank(params): Observable<UserPopularResponse[]> {
        return this.http.get<any>(`${this.apiUrlPublic}/getRank`, { params });
    }

}
