import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ReportRequest } from '../models/request/report.request';
import { ReportResponse } from '../models/response/report.response';

@Injectable({
    providedIn: 'root'
})
export class ReportService {
    private apiUrl = environment.apiUrl + 'api/reports';

    constructor(
        private http: HttpClient
    ) { }

    send(report: ReportRequest) {
        return this.http.post<any>(`${this.apiUrl}`, report);
    }

    getAll(params): Observable<ReportResponse[]> {
        return this.http.get<any>(`${this.apiUrl}`, { params });
    }

    resolve(reportId: string) {
        return this.http.put<any>(`${this.apiUrl}/resolve/${reportId}`, {});
    }

}
