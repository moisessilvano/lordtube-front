import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageRequest } from '../models/request/page.request';
import { FaqItemResponse } from '../models/response/faq-item.response';
import { PageResponse } from '../models/response/page.response';
import { PartnerRequest } from '../models/request/partner.request';
import { PartnerResponse } from '../models/response/partner.response';
import { AdventRequest } from '../models/request/advent.request';
import { AdventResponse } from '../models/response/advent.response';

@Injectable({
    providedIn: 'root'
})
export class AdventService {
    private apiUrl = environment.apiUrl + 'api/advents';

    constructor(
        private http: HttpClient
    ) { }

    create(advent: AdventRequest) {
        return this.http.post<any>(`${this.apiUrl}`, advent);
    }

    update(id: string, advent: AdventRequest) {
        return this.http.put<any>(`${this.apiUrl}/${id}`, advent);
    }

    getAll(params): Observable<AdventResponse[]> {
        return this.http.get<any>(`${this.apiUrl}`, { params });
    }

    getById(id: string): Observable<AdventResponse> {
        return this.http.get<any>(`${this.apiUrl}/${id}`);
    }

    getAds(): Observable<AdventResponse> {
        return this.http.get<any>(`${this.apiUrl}/getAds`);
    }

}
