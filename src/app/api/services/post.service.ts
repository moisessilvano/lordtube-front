import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { PostRequest } from '../models/request/post.request';
import { PostResponse } from '../models/response/post.response';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private apiUrl = environment.apiUrl + 'api/posts';

  constructor(
    private http: HttpClient
  ) { }

  createPost(post: PostRequest) {
    return this.http.post<any>(`${this.apiUrl}`, post);
  }

  getPostById(postId: string): Observable<PostResponse> {
    return this.http.get<any>(`${this.apiUrl}/${postId}`);
  }

  getPosts(params): Observable<PostResponse[]> {
    return this.http.get<any>(`${this.apiUrl}`, { params: params });
  }

  getPostsByUser(userId: string, params): Observable<PostResponse[]> {
    return this.http.get<any>(`${this.apiUrl}/getByUser/${userId}`, { params: params });
  }

}
