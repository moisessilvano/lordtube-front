import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-youtube-page',
  templateUrl: './youtube-page.component.html',
  styleUrls: ['./youtube-page.component.scss']
})
export class YoutubePageComponent implements OnInit {

  constructor(
    private titleService: Title,
    private meta: Meta
  ) { }

  ngOnInit() {
    this.titleService.setTitle('LordTube | Divulgue seu canal do YouTube');
    this.meta.addTag({ name: 'keywords', content: 'videos, inscritos, seguidores, subscribe, canal, gostei, joinha, youtuber, youtuber, influenciador' });
    this.meta.addTag({ name: 'description', content: 'Uma rede social feita para influenciadors digitais, divulgue seus conteúdos, faça novas amizades e começe sua jornada como um influenciador digital' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

    this.meta.addTag({ property: 'og:title', content: 'LordTube | Divulgue seu canal do YouTube' });
    this.meta.addTag({ property: 'og:description', content: 'Uma rede social feita para influenciadors digitais, divulgue seus conteúdos, faça novas amizades e começe sua jornada como um influenciador digital' });
    this.meta.addTag({ property: 'og:image', content: 'https://lordtube.com/assets/img/lordtube-youtube.png' });
  }

}
