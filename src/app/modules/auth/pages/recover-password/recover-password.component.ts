import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/api/services/auth.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
  submitted: Boolean;
  recoverForm: FormGroup;
  captchaResponse: string;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private titleService: Title,
    private meta: Meta
  ) {
    this.titleService.setTitle('Recuperar Senha | LordTube');
    this.meta.addTag({ name: 'keywords', content: 'videos, divulgacao, youtube, rede social, lordtube, lorditubi, lordetube' });
    this.meta.addTag({ name: 'description', content: 'Esqueceu sua senha? Não se preocupe, siga o passo a passo indicado e acesse novamente nossa plataforma.' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

    this.meta.addTag({ property: 'og:title', content: 'Recuperar Senha | LordTube' });
    this.meta.addTag({ property: 'og:description', content: 'Esqueceu sua senha? Não se preocupe, siga o passo a passo indicado e acesse novamente nossa plataforma.' });
    this.meta.addTag({ property: 'og:image', content: 'https://lordtube.com/assets/img/logo-app.png' });
  }

  ngOnInit() {
    this.startForm();
  }

  startForm() {
    this.recoverForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get f() { return this.recoverForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.recoverForm.invalid) {
      this.toastr.error('Preencha todos os campos!');

    } else {
      const { email } = this.recoverForm.value;

      this.authService.recoverPassword({
        email: email,
        recaptcha: this.captchaResponse
      })
        .subscribe(res => {
          this.toastr.success(res.message);
          return res;
        }, err => {
          this.toastr.error('Erro ao efetuar login!');
          return err;
        });
    }
  }

  infoCaptchaToken(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }

}
