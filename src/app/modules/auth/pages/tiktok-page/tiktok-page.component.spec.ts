import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiktokPageComponent } from './tiktok-page.component';

describe('TiktokPageComponent', () => {
  let component: TiktokPageComponent;
  let fixture: ComponentFixture<TiktokPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiktokPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiktokPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
