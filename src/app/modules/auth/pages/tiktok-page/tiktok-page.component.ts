import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-tiktok-page',
  templateUrl: './tiktok-page.component.html',
  styleUrls: ['./tiktok-page.component.scss']
})
export class TiktokPageComponent implements OnInit {

  constructor(
    private titleService: Title,
    private meta: Meta
  ) { }

  ngOnInit() {
    this.titleService.setTitle('LordTube | Divulgue seu TikTok');
    this.meta.addTag({ name: 'keywords', content: 'videos, tiktok, challenge, divulgação, seguidores' });
    this.meta.addTag({ name: 'description', content: 'Uma rede social feita para influenciadors digitais, divulgue seus conteúdos, faça novas amizades e começe sua jornada como um influenciador digital' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

    this.meta.addTag({ property: 'og:title', content: 'LordTube | Divulgue seu TikTok' });
    this.meta.addTag({ property: 'og:description', content: 'Uma rede social feita para influenciadors digitais, divulgue seus conteúdos, faça novas amizades e começe sua jornada como um influenciador digital' });
    this.meta.addTag({ property: 'og:image', content: 'https://lordtube.com/assets/img/lordtube-tiktok.png' });
  }

}
