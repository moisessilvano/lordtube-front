import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/api/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  submitted: Boolean = false;
  passwordShow: Boolean;
  confirmPasswordShow: boolean;
  registerForm: FormGroup;
  captchaResponse: string;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private titleService: Title,
    private meta: Meta
  ) {
    this.titleService.setTitle('Crie sua conta no LordTube');
    this.meta.addTag({ name: 'keywords', content: 'videos, divulgacao, youtube, rede social, lordtube, lorditubi, lordetube' });
    this.meta.addTag({ name: 'description', content: 'Crie sua conta gratuitamente agora mesmo e tenha acesso a todas as funcionalidades do LordTube' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

    this.meta.addTag({ property: 'og:title', content: 'Crie sua conta no LordTube' });
    this.meta.addTag({ property: 'og:description', content: 'Crie sua conta gratuitamente agora mesmo e tenha acesso a todas as funcionalidades do LordTube' });
    this.meta.addTag({ property: 'og:image', content: 'https://lordtube.com/assets/img/logo-app.png' });
  }

  private passwordMatcher(control: FormControl): { [s: string]: boolean } {
    if (
      this.registerForm &&
      (control.value !== this.registerForm.controls.password.value)
    ) {
      return { passwordNotMatch: true };
    }
    return null;
  }


  ngOnInit() {
    this.startForm();
  }

  startForm() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required, this.passwordMatcher.bind(this)]],
      name: ['', Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      this.toastr.error('Preencha todos os campos!');

    } else {
      this.authService.registerUser({
        ...this.registerForm.value,
        recaptcha: this.captchaResponse
      })
        .subscribe(res => {
          this.toastr.success(res.message);
          this.router.navigate(['/login']);
          return res;
        }, err => {
          if (err.error.length > 0) {
            for (let i = 0; i < err.error.length; i++) {
              this.toastr.error(err.error[i].message);
            }
          } else {
            this.toastr.error(err.error.message);
          }

          return err;
        });
    }
  }

  togglePasswordShow() {
    this.passwordShow = !this.passwordShow;
  }

  toggleConfirmPasswordShow() {
    this.confirmPasswordShow = !this.confirmPasswordShow;
  }

  infoCaptchaToken(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}
