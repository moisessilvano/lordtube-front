import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private titleService: Title,
    private meta: Meta
  ) { }

  ngOnInit() {
    this.titleService.setTitle('LordTube | Rede Social para Influenciadores Digitais');
    this.meta.addTag({ name: 'keywords', content: 'lordtube, lorditubi, lordetube, influenciador, youtuber, youtube, tiktok, instagram, facebook' });
    this.meta.addTag({ name: 'description', content: 'Uma rede social feita para influenciadors digitais, divulgue seus conteúdos, faça novas amizades e começe sua jornada como um influenciador digital' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

    this.meta.addTag({ property: 'og:title', content: 'LordTube | Rede Social para Influenciadores Digitais' });
    this.meta.addTag({ property: 'og:description', content: 'Uma rede social feita para influenciadors digitais, divulgue seus conteúdos, faça novas amizades e começe sua jornada como um influenciador digital' });
    this.meta.addTag({ property: 'og:image', content: 'https://lordtube.com/assets/img/logo-app.png' });
  }

}
