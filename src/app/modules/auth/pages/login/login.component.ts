import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/api/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  submitted: Boolean;
  passwordShow: Boolean;

  authForm: FormGroup;

  captchaResponse: string;
  attemptsCount: number;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private titleService: Title,
    private meta: Meta
  ) {
    this.titleService.setTitle('Entre no LordTube');
    this.meta.addTag({ name: 'keywords', content: 'videos, divulgacao, youtube, rede social, lordtube, lorditubi, lordetube' });
    this.meta.addTag({ name: 'description', content: 'Bem vindo de volta ao LordTube. Conecte-se conosco e fique por dentro de tudo que está rolando aqui' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

    this.meta.addTag({ property: 'og:title', content: 'Entre no LordTube' });
    this.meta.addTag({ property: 'og:description', content: 'Bem vindo de volta ao LordTube. Conecte-se conosco e fique por dentro de tudo que está rolando aqui' });
    this.meta.addTag({ property: 'og:image', content: 'https://lordtube.com/assets/img/logo-app.png' });
  }

  ngOnInit() {
    this.startForm();

    const attemptsCount = localStorage.getItem('attemptsCount');
    if (attemptsCount) {
      this.attemptsCount = Number(attemptsCount);
    }
  }

  startForm() {
    this.authForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  get f() { return this.authForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.authForm.invalid) {
      this.toastr.error('Preencha todos os campos!');

    } else {
      this.authService.auth({
        ...this.authForm.value,
        recaptcha: this.captchaResponse
      })
        .subscribe(res => {
          location.href = '/';
          return res;
        }, err => {
          this.submitted = false;

          const { error } = err;
          if (!error) return;

          if (error && error.message) {
            this.toastr.error(error.message);
          }

          return err;
        });
    }
  }

  togglePasswordShow() {
    this.passwordShow = !this.passwordShow;
  }

}
