import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { IndexComponent } from './pages/index/index.component';
import { LoginComponent } from './pages/login/login.component';
import { RecoverPasswordComponent } from './pages/recover-password/recover-password.component';
import { RegisterComponent } from './pages/register/register.component';
import { TiktokPageComponent } from './pages/tiktok-page/tiktok-page.component';
import { YoutubePageComponent } from './pages/youtube-page/youtube-page.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'tiktok', component: TiktokPageComponent },
  { path: 'youtube', component: YoutubePageComponent },
  {
    path: '', component: IndexComponent, children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'recover-password', component: RecoverPasswordComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
