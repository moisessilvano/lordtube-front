import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { RecoverPasswordComponent } from './pages/recover-password/recover-password.component';
import { RegisterComponent } from './pages/register/register.component';
import { PasswordStrengthMeterModule } from 'angular-password-strength-meter';
import { HomeComponent } from './pages/home/home.component';
import { IndexComponent } from './pages/index/index.component';
import { TiktokPageComponent } from './pages/tiktok-page/tiktok-page.component';
import { YoutubePageComponent } from './pages/youtube-page/youtube-page.component';

@NgModule({
  declarations: [LoginComponent, RecoverPasswordComponent, RegisterComponent, HomeComponent, IndexComponent, TiktokPageComponent, YoutubePageComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    PasswordStrengthMeterModule
  ]
})
export class AuthModule { }
