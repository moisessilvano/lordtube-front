import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { IndexComponent } from './pages/index/index.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UpdateDataComponent } from './components/update-data/update-data.component';
import { UpdatePasswordComponent } from './components/update-password/update-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PasswordComponent } from './pages/password/password.component';
import { MyDataComponent } from './pages/my-data/my-data.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdatePrivacyComponent } from './components/update-privacy/update-privacy.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { PasswordStrengthMeterModule } from 'angular-password-strength-meter';

@NgModule({
  declarations: [IndexComponent, UpdateDataComponent, UpdatePasswordComponent, NavigationComponent, PasswordComponent, MyDataComponent, UpdatePrivacyComponent, PrivacyComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    PasswordStrengthMeterModule
  ]
})
export class SettingsModule { }
