import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/api/services/user.service';
import { UserResponse } from 'src/app/api/models/response/user.response';
import { NgbDateStruct, NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePickerAdapter } from 'src/app/shared/utils/datepicker-adapter';
import { DateParserFormatter } from 'src/app/shared/utils/date-parser-formatter';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';
import { EventEmitterService } from 'src/app/shared/services/event-emitter.service';

@Component({
  selector: 'app-update-data',
  templateUrl: './update-data.component.html',
  styleUrls: ['./update-data.component.scss'],
  providers: [
    { provide: NgbDateAdapter, useClass: DatePickerAdapter },
    { provide: NgbDateParserFormatter, useClass: DateParserFormatter }
  ]
})
export class UpdateDataComponent implements OnInit {
  userForm: FormGroup;
  user: UserResponse;

  isLoading: boolean;

  hasText: boolean;

  constructor(
    private userService: UserService,
    private defaultToastrService: DefaultToastrService
  ) { }

  ngOnInit() {
    this.startForm();
    this.getUser();

    this.userForm.get('description').valueChanges.subscribe(res => {
      if (res.length > 0) {
        this.hasText = true;
      } else {
        this.hasText = false;
      }
    })
  }

  startForm() {
    this.userForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required]),
      description: new FormControl('')
    });
  }

  get f() { return this.userForm.controls; }

  getUser() {
    this.isLoading = true;
    return this.userService.getCurrentUser()
      .subscribe(res => {
        this.user = res;
        this.userForm.patchValue(this.user);
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
      });
  }

  onSubmit() {
    if (this.userForm.invalid) {
      this.defaultToastrService.error('Formulário inválido!');
      return;
    }

    this.isLoading = true;

    const { value } = this.userForm;

    this.userService.updateData(this.userForm.value).subscribe(res => {
      this.defaultToastrService.success('Dados atualizados com sucesso!');
      EventEmitterService.get('updatedUser').emit(value);
      this.isLoading = false;
    }, err => {
      this.defaultToastrService.error('Não foi possível atualizar');
      this.isLoading = false;
    })
  }

}
