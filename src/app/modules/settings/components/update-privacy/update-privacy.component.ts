import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserResponse } from 'src/app/api/models/response/user.response';

@Component({
  selector: 'app-update-privacy',
  templateUrl: './update-privacy.component.html',
  styleUrls: ['./update-privacy.component.scss']
})
export class UpdatePrivacyComponent implements OnInit {
  privacyForm: FormGroup;
  user: UserResponse;

  constructor() { }

  ngOnInit() {
    this.startForm();
  }

  startForm() {
    this.privacyForm = new FormGroup({
      profile: new FormControl('', [Validators.required]),
    });
  }

  get f() { return this.privacyForm.controls; }

  onSubmit() {

  }

}
