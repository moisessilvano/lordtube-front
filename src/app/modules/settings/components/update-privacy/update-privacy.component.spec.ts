import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePrivacyComponent } from './update-privacy.component';

describe('UpdatePrivacyComponent', () => {
  let component: UpdatePrivacyComponent;
  let fixture: ComponentFixture<UpdatePrivacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePrivacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
