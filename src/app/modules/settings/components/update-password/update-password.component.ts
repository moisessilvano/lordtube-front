import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/api/services/user.service';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  passwordForm: FormGroup;

  oldPasswordShow: boolean;
  newPasswordShow: boolean;
  newPasswordConfirmShow: boolean;

  isLoading: boolean;

  private passwordMatcher(control: FormControl): { [s: string]: boolean } {
    if (
      this.passwordForm &&
      (control.value !== this.passwordForm.controls.newPassword.value)
    ) {
      return { passwordNotMatch: true };
    }
    return null;
  }

  constructor(
    private userService: UserService,
    private defaultToastrService: DefaultToastrService
  ) { }

  ngOnInit() {
    this.startForm();
  }

  startForm() {
    this.passwordForm = new FormGroup({
      oldPassword: new FormControl('', Validators.required),
      newPassword: new FormControl('', Validators.required),
      newPasswordConfirm: new FormControl('', [Validators.required, this.passwordMatcher.bind(this)]),
    });
  }

  get f() { return this.passwordForm.controls; }

  onSubmit() {
    if (this.passwordForm.invalid) {
      this.defaultToastrService.error('Formulário inválido!');
      return;
    }

    this.isLoading = true;

    this.userService.updatePassword(this.passwordForm.value).subscribe(res => {
      this.defaultToastrService.success('Senha atualizada com sucesso!');
      this.isLoading = false;
    }, err => {
      this.defaultToastrService.error('Algo de errado aconteceu!');
      this.isLoading = false;
    })
  }

  toggleOldPasswordShow() {
    this.oldPasswordShow = !this.oldPasswordShow;
  }

  toggleNewPasswordShow() {
    this.newPasswordShow = !this.newPasswordShow;
  }

  toggleNewPasswordConfirmShow() {
    this.newPasswordConfirmShow = !this.newPasswordConfirmShow;
  }

}
