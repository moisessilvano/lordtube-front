import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { MyDataComponent } from './pages/my-data/my-data.component';
import { PasswordComponent } from './pages/password/password.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';


const routes: Routes = [
  {
    path: '', component: IndexComponent, children: [
      { path: '', component: MyDataComponent },
      { path: 'password', component: PasswordComponent },
      { path: 'privacy', component: PrivacyComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
