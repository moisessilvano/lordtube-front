import { Component, OnInit } from '@angular/core';
import { FaqItemResponse } from 'src/app/api/models/response/faq-item.response';
import { FaqItemService } from 'src/app/api/services/faq.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditFaqItemModalComponent } from '../../modals/edit-faq-item-modal/edit-faq-item-modal.component';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  faqItems: FaqItemResponse[] = [];

  limit: number = 15;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  constructor(
    private faqItemService: FaqItemService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getFaqItems();
  }

  getInitialData() {
    this.limit = 15;
    this.skip = 0;
    this.isFinish = false;
    this.faqItems = [];
    this.getFaqItems();
  }

  getFaqItems() {
    if (this.isLoading || this.isFinish) return;

    this.isLoading = true;
    this.faqItemService.getAll({ limit: this.limit, skip: this.skip })
      .subscribe(res => {
        if (res.length === 0) {
          this.isFinish = true;
        }

        this.skip += this.limit;
        this.faqItems = [...this.faqItems, ...res];
        this.isLoading = false;
      })
  }

  createFaqItem() {
    const modalRef = this.modalService.open(EditFaqItemModalComponent);
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

  editFaqItem(id: string) {
    const modalRef = this.modalService.open(EditFaqItemModalComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

}
