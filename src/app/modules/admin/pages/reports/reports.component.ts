import { Component, OnInit } from "@angular/core";
import { ReportService } from "src/app/api/services/report.service";
import { ReportResponse } from "src/app/api/models/response/report.response";

@Component({
  selector: "app-reports",
  templateUrl: "./reports.component.html",
  styleUrls: ["./reports.component.scss"],
})
export class ReportsComponent implements OnInit {
  reports: ReportResponse[] = [];
  status: boolean;

  limit: number = 15;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  constructor(private reportService: ReportService) { }

  ngOnInit() {
    this.getReportsByStatus(false);
  }

  getReports() {
    if (this.isLoading || this.isFinish) return;

    this.isLoading = true;

    this.reportService
      .getAll({ status: this.status, limit: this.limit, skip: this.skip })
      .subscribe((res) => {
        if (res.length === 0) {
          this.isFinish = true;
        }

        this.skip += this.limit;
        this.reports = [...this.reports, ...res];
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
      });
  }

  getReportsByStatus(status) {
    this.status = status;
    this.reports = [];
    this.skip = 0;
    this.getReports();
  }

  resolveReport(id: string) {
    this.reportService.resolve(id).subscribe((res) => {
      this.reports = this.reports.filter((r) => r._id != id);
      console.log("resolvido com sucesso");
    });
  }
}
