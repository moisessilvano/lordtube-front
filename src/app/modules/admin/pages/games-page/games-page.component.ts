import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/api/services/game.service';
import { GameResponse } from 'src/app/api/models/response/game.response';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditAdventModalComponent } from '../../modals/edit-advent-modal/edit-advent-modal.component';
import { EditPageModalComponent } from '../../modals/edit-page-modal/edit-page-modal.component';
import { EditGameModalComponent } from '../../modals/edit-game-modal/edit-game-modal.component';

@Component({
  selector: 'app-games-page',
  templateUrl: './games-page.component.html',
  styleUrls: ['./games-page.component.scss']
})
export class GamesPageComponent implements OnInit {


  games: GameResponse[] = [];

  limit: number = 15;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  constructor(
    private gameService: GameService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getInitialData();
  }

  getInitialData() {
    this.limit = 50;
    this.skip = 0;
    this.isFinish = false;
    this.games = [];
    this.getGames();
  }

  getGames() {
    if (this.isLoading || this.isFinish) return;
    this.isLoading = true;

    this.gameService.getAll({ limit: this.limit, skip: this.skip })
      .subscribe(res => {
        this.isLoading = false;

        if (res.length == 0) {
          this.isFinish = true;
          return;
        }

        this.skip += this.limit;
        this.games = [...this.games, ...res];
      })
  }

  create() {
    const modalRef = this.modalService.open(EditGameModalComponent);
    modalRef.result.then(res => {
      if (res == 'updated') {
        this.getInitialData();
      }
    });
  }

  edit(id: string) {
    const modalRef = this.modalService.open(EditGameModalComponent)
    modalRef.componentInstance.id = id;
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

}
