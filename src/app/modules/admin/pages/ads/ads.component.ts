import { Component, OnInit } from '@angular/core';
import { AdventResponse } from 'src/app/api/models/response/advent.response';
import { AdventService } from 'src/app/api/services/advent.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditAdventModalComponent } from '../../modals/edit-advent-modal/edit-advent-modal.component';

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {


  advents: AdventResponse[] = [];

  limit: number = 15;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  constructor(
    private advenetService: AdventService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getInitialData();
  }

  getInitialData() {
    this.limit = 30;
    this.skip = 0;
    this.isFinish = false;
    this.advents = [];
    this.getAdvents();
  }

  getAdvents() {
    if (this.isLoading || this.isFinish) return;
    this.isLoading = true;

    this.advenetService.getAll({ limit: this.limit, skip: this.skip })
      .subscribe(res => {
        this.isLoading = false;

        if (res.length == 0) {
          this.isFinish = true;
          return;
        }

        this.skip += this.limit;
        this.advents = [...this.advents, ...res];
      })
  }

  create() {
    const modalRef = this.modalService.open(EditAdventModalComponent);
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

  edit(id: string) {
    const modalRef = this.modalService.open(EditAdventModalComponent)
    modalRef.componentInstance.id = id;
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

}
