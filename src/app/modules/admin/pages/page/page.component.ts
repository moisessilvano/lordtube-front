import { Component, OnInit } from '@angular/core';
import { FaqItemResponse } from 'src/app/api/models/response/faq-item.response';
import { FaqItemService } from 'src/app/api/services/faq.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditFaqItemModalComponent } from '../../modals/edit-faq-item-modal/edit-faq-item-modal.component';
import { PageResponse } from 'src/app/api/models/response/page.response';
import { PageService } from 'src/app/api/services/page.service';
import { EditPageModalComponent } from '../../modals/edit-page-modal/edit-page-modal.component';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  pages: PageResponse[] = [];

  limit: number = 15;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  constructor(
    private pageService: PageService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getInitialData();
  }

  getInitialData() {
    this.limit = 15;
    this.skip = 0;
    this.isFinish = false;
    this.pages = [];
    this.getPages();
  }

  getPages() {
    if (this.isLoading || this.isFinish) return;

    this.isLoading = true;
    this.pageService.getAll({ limit: this.limit, skip: this.skip })
      .subscribe(res => {
        if (res.length === 0) {
          this.isFinish = true;
        }

        this.skip += this.limit;
        this.pages = [...this.pages, ...res];
        this.isLoading = false;
      })
  }

  create() {
    const modalRef = this.modalService.open(EditPageModalComponent);
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

  edit(id: string) {
    const modalRef = this.modalService.open(EditPageModalComponent)
    modalRef.componentInstance.id = id;
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

}
