import { Component, OnInit } from '@angular/core';
import { PartnerService } from 'src/app/api/services/partner.service';
import { PartnerResponse } from 'src/app/api/models/response/partner.response';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditPartnerModalComponent } from '../../modals/edit-partner-modal/edit-partner-modal.component';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {

  partners: PartnerResponse[] = [];

  limit: number = 15;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  constructor(
    private partnerService: PartnerService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getInitialData();
  }

  getInitialData() {
    this.limit = 15;
    this.skip = 0;
    this.isFinish = false;
    this.partners = [];
    this.getPartners();
  }

  getPartners() {
    if (this.isLoading || this.isFinish) return;

    this.isLoading = true;
    this.partnerService.getAll({ limit: this.limit, skip: this.skip })
      .subscribe(res => {
        if (res.length === 0) {
          this.isFinish = true;
        }

        this.skip += this.limit;
        this.partners = [...this.partners, ...res];
        this.isLoading = false;
      })
  }

  create() {
    const modalRef = this.modalService.open(EditPartnerModalComponent);
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

  edit(id: string) {
    const modalRef = this.modalService.open(EditPartnerModalComponent)
    modalRef.componentInstance.id = id;
    modalRef.result.then(() => {
      this.getInitialData();
    });
  }

}
