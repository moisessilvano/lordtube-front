import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolveReportComponent } from './resolve-report.component';

describe('ResolveReportComponent', () => {
  let component: ResolveReportComponent;
  let fixture: ComponentFixture<ResolveReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolveReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolveReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
