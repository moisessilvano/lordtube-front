import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PageService } from 'src/app/api/services/page.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-edit-page-modal',
  templateUrl: './edit-page-modal.component.html',
  styleUrls: ['./edit-page-modal.component.scss']
})
export class EditPageModalComponent implements OnInit {
  @Input() id;
  items: FormArray;

  form = new FormGroup({
    path: new FormControl('', Validators.required),
    status: new FormControl(true, Validators.required),
    isPublic: new FormControl(false, Validators.required),
    translations: new FormArray([]),
  });

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    width: '100%',
    height: 'auto',
    placeholder: 'No que você está pensando?',
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: false,
    toolbarPosition: 'top',
    toolbarHiddenButtons: []
  };

  constructor(
    private pageService: PageService,
    public modal: NgbActiveModal
  ) { }

  ngOnInit() {
    !this.id ? this.addTranslation() : this.getById();
  }

  get translations(): FormArray {
    return this.form.get('translations') as FormArray;
  }

  addTranslation() {
    this.translations.push(new FormGroup({
      language: new FormControl(''),
      title: new FormControl(''),
      tags: new FormControl(''),
      content: new FormControl(''),
    }));
  }

  removeTranslation(index: number) {
    this.translations.removeAt(index);
  }

  getById() {
    this.pageService.getById(this.id).subscribe(res => {
      res.translations.map(() => this.addTranslation());
      this.form.patchValue(res);
    })
  }

  onSubmit() {
    if (this.form.invalid) return;

    if (this.id) {
      this.pageService.update(this.id, this.form.value).subscribe(res => {
        console.log('editado com sucesso');
        this.closeModal();
      })
    } else {
      this.pageService.create(this.form.value).subscribe(res => {
        console.log('criado com sucesso');
        this.closeModal();
      })
    }
  }

  closeModal() {
    this.modal.close();
  }

}
