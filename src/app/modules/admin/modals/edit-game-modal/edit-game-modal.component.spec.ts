import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGameModalComponent } from './edit-game-modal.component';

describe('EditGameModalComponent', () => {
  let component: EditGameModalComponent;
  let fixture: ComponentFixture<EditGameModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGameModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGameModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
