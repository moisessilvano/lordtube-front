import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GameService } from 'src/app/api/services/game.service';

@Component({
  selector: 'app-edit-game-modal',
  templateUrl: './edit-game-modal.component.html',
  styleUrls: ['./edit-game-modal.component.scss']
})
export class EditGameModalComponent implements OnInit {
  @Input() id;

  form = new FormGroup({
    title: new FormControl('', Validators.required),
    link: new FormControl(''),
    image: new FormControl(''),
    category: new FormControl(''),
    status: new FormControl(true, Validators.required),
  });

  constructor(
    private gameService: GameService,
    public modal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.id && this.getById();
  }

  getById() {
    this.gameService.getById(this.id).subscribe(res => {
      this.form.patchValue(res);
    })
  }

  onSubmit() {
    if (this.form.invalid) return;

    if (this.id) {
      this.gameService.update(this.id, this.form.value).subscribe(res => {
        this.modal.close('updated');
      })
    } else {
      this.gameService.create(this.form.value).subscribe(res => {
        this.modal.close('updated');
      })
    }
  }

  closeModal() {
    this.modal.close();
  }

}
