import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPartnerModalComponent } from './edit-partner-modal.component';

describe('EditPartnerModalComponent', () => {
  let component: EditPartnerModalComponent;
  let fixture: ComponentFixture<EditPartnerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPartnerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPartnerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
