import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PartnerService } from 'src/app/api/services/partner.service';

@Component({
  selector: 'app-edit-partner-modal',
  templateUrl: './edit-partner-modal.component.html',
  styleUrls: ['./edit-partner-modal.component.scss']
})
export class EditPartnerModalComponent implements OnInit {
  @Input() id;

  form = new FormGroup({
    link: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required),
    banner: new FormControl('', Validators.required),
    status: new FormControl(true, Validators.required),
  });

  constructor(
    private partnerService: PartnerService,
    public modal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.id && this.getById();
  }

  getById() {
    this.partnerService.getById(this.id).subscribe(res => {
      this.form.patchValue(res);
    })
  }

  onSubmit() {
    if (this.form.invalid) return;

    if (this.id) {
      this.partnerService.update(this.id, this.form.value).subscribe(res => {
        console.log('editado com sucesso');
        this.closeModal();
      })
    } else {
      this.partnerService.create(this.form.value).subscribe(res => {
        console.log('criado com sucesso');
        this.closeModal();
      })
    }
  }

  closeModal() {
    this.modal.close();
  }


}
