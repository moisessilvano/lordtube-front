import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAdventModalComponent } from './edit-advent-modal.component';

describe('EditAdventModalComponent', () => {
  let component: EditAdventModalComponent;
  let fixture: ComponentFixture<EditAdventModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAdventModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAdventModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
