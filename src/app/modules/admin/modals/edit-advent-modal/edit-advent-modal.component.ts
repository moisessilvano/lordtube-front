import { Component, OnInit, Input } from '@angular/core';
import { AdventService } from 'src/app/api/services/advent.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit-advent-modal',
  templateUrl: './edit-advent-modal.component.html',
  styleUrls: ['./edit-advent-modal.component.scss']
})
export class EditAdventModalComponent implements OnInit {
  @Input() id;

  form = new FormGroup({
    title: new FormControl('', Validators.required),
    link: new FormControl(''),
    image: new FormControl(''),
    htmlCode: new FormControl(''),
    type: new FormControl('', Validators.required),
    status: new FormControl(true, Validators.required),
  });

  constructor(
    private adventService: AdventService,
    public modal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.id && this.getById();
  }


  getById() {
    this.adventService.getById(this.id).subscribe(res => {
      this.form.patchValue(res);
    })
  }

  onSubmit() {
    if (this.form.invalid) return;

    if (this.id) {
      this.adventService.update(this.id, this.form.value).subscribe(res => {
        console.log('editado com sucesso');
        this.closeModal();
      })
    } else {
      this.adventService.create(this.form.value).subscribe(res => {
        console.log('criado com sucesso');
        this.closeModal();
      })
    }
  }

  closeModal() {
    this.modal.close();
  }


}
