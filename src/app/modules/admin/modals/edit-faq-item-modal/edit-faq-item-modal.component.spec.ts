import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFaqItemModalComponent } from './edit-faq-item-modal.component';

describe('EditFaqItemModalComponent', () => {
  let component: EditFaqItemModalComponent;
  let fixture: ComponentFixture<EditFaqItemModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFaqItemModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFaqItemModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
