import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { FaqItemService } from 'src/app/api/services/faq.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FaqItemResponse } from 'src/app/api/models/response/faq-item.response';

@Component({
  selector: 'app-edit-faq-item-modal',
  templateUrl: './edit-faq-item-modal.component.html',
  styleUrls: ['./edit-faq-item-modal.component.scss']
})
export class EditFaqItemModalComponent implements OnInit {
  @Input() id;
  items: FormArray;

  faqItemResponse: FaqItemResponse;

  form = new FormGroup({
    path: new FormControl('', Validators.required),
    status: new FormControl(true, Validators.required),
    translations: new FormArray([]),
  });

  constructor(
    private faqItemService: FaqItemService,
    public modal: NgbActiveModal
  ) { }

  ngOnInit() {
    !this.id ? this.addTranslation() : this.getById();
  }

  get translations(): FormArray {
    return this.form.get('translations') as FormArray;
  }

  addTranslation() {
    this.translations.push(new FormGroup({
      language: new FormControl(''),
      title: new FormControl(''),
      tags: new FormControl(''),
      content: new FormControl(''),
    }));
  }

  removeTranslation(index: number) {
    this.translations.removeAt(index);
  }

  getById() {
    this.faqItemService.getById(this.id).subscribe(faqItem => {
      faqItem.translations.map(() => this.addTranslation());
      this.form.patchValue(faqItem);
    })
  }

  onSubmit() {
    if (this.form.invalid) return;

    if (this.id) {
      this.faqItemService.update(this.id, this.form.value).subscribe(res => {
        console.log('editado com sucesso');
        this.closeModal();
      })
    } else {
      this.faqItemService.create(this.form.value).subscribe(res => {
        console.log('criado com sucesso');
        this.closeModal();
      })
    }
  }

  closeModal() {
    this.modal.close();
  }

}
