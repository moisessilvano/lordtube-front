import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { HomeComponent } from './pages/home/home.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { PartnersComponent } from './pages/partners/partners.component';
import { NotifyComponent } from './pages/notify/notify.component';
import { FaqComponent } from './pages/faq/faq.component';
import { DataComponent } from './pages/data/data.component';
import { AdsComponent } from './pages/ads/ads.component';
import { AboutComponent } from './pages/about/about.component';
import { UsersComponent } from './pages/users/users.component';
import { PageComponent } from './pages/page/page.component';
import { GamesPageComponent } from './pages/games-page/games-page.component';


const routes: Routes = [
  {
    path: '', component: IndexComponent, children: [
      { path: '', component: HomeComponent },
      { path: 'reports', component: ReportsComponent },
      { path: 'users', component: UsersComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'partners', component: PartnersComponent },
      { path: 'notify', component: NotifyComponent },
      { path: 'faq', component: FaqComponent },
      { path: 'page', component: PageComponent },
      { path: 'data', component: DataComponent },
      { path: 'ads', component: AdsComponent },
      { path: 'about', component: AboutComponent },
      { path: 'games', component: GamesPageComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
