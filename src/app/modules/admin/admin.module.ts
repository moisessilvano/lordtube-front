import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AdminRoutingModule } from "./admin-routing.module";
import { ReportsComponent } from "./pages/reports/reports.component";
import { UsersComponent } from "./pages/users/users.component";
import { FaqComponent } from "./pages/faq/faq.component";
import { AboutComponent } from "./pages/about/about.component";
import { PartnersComponent } from "./pages/partners/partners.component";
import { DataComponent } from "./pages/data/data.component";
import { SettingsComponent } from "./pages/settings/settings.component";
import { IndexComponent } from "./pages/index/index.component";
import { MenuComponent } from "./components/menu/menu.component";
import { AdsComponent } from "./pages/ads/ads.component";
import { NotifyComponent } from "./pages/notify/notify.component";
import { ResolveReportComponent } from "./modals/resolve-report/resolve-report.component";
import { ReportComponent } from "./components/report/report.component";
import { HomeComponent } from "./pages/home/home.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "src/app/shared/shared.module";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { EditFaqItemModalComponent } from './modals/edit-faq-item-modal/edit-faq-item-modal.component';
import { EditPageModalComponent } from './modals/edit-page-modal/edit-page-modal.component';
import { PageComponent } from './pages/page/page.component';
import { EditPartnerModalComponent } from './modals/edit-partner-modal/edit-partner-modal.component';
import { EditAdventModalComponent } from './modals/edit-advent-modal/edit-advent-modal.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { EditGameModalComponent } from './modals/edit-game-modal/edit-game-modal.component';
import { GamesPageComponent } from './pages/games-page/games-page.component';

@NgModule({
  declarations: [
    ReportsComponent,
    UsersComponent,
    FaqComponent,
    PageComponent,
    AboutComponent,
    PartnersComponent,
    DataComponent,
    SettingsComponent,
    IndexComponent,
    MenuComponent,
    AdsComponent,
    NotifyComponent,
    ResolveReportComponent,
    ReportComponent,
    HomeComponent,
    EditFaqItemModalComponent,
    EditPageModalComponent,
    EditPartnerModalComponent,
    EditAdventModalComponent,
    EditGameModalComponent,
    GamesPageComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InfiniteScrollModule,
    AngularEditorModule
  ],
  entryComponents: [
    EditFaqItemModalComponent,
    EditPageModalComponent,
    EditPartnerModalComponent,
    EditAdventModalComponent,
    EditGameModalComponent
  ]
})
export class AdminModule { }
