import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LordtalkRoutingModule } from './lordtalk-routing.module';
import { ChatComponent } from './components/chat/chat.component';
import { TalksComponent } from './components/talks/talks.component';
import { HomeComponent } from './pages/home/home.component';
import { IndexComponent } from './pages/index/index.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
  declarations: [ChatComponent, TalksComponent, HomeComponent, IndexComponent],
  imports: [
    CommonModule,
    LordtalkRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PipesModule,
    NgbModule,
    InfiniteScrollModule
  ]
})
export class LordtalkModule { }
