import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompileShallowModuleMetadata } from '@angular/compiler';
import { ChatRoomService } from 'src/app/api/services/chat-room.service';
import { ChatRoomResponse } from 'src/app/api/models/response/chat-room.response';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  roomId: any;
  @Input() room: ChatRoomResponse;

  constructor(
    private route: ActivatedRoute,
    private chatRoomService: ChatRoomService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(param => {
      this.room = null;
      this.roomId = param["room"];
      if (this.roomId) {
        this.getRoomById();
      }
    })
  }

  ngOnDestroy() {
    this.room = null;
  }

  getRoomById() {
    this.chatRoomService.getRoomById(this.roomId).subscribe(res => {
      this.room = res;
    });
  }

}
