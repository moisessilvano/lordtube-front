import { Component, Input, OnDestroy, OnInit, ViewChild, HostListener, AfterContentChecked, AfterContentInit, AfterViewInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChatMessageResponse } from 'src/app/api/models/response/chat-message.response';
import { ChatRoomResponse } from 'src/app/api/models/response/chat-room.response';
import { ChatMessageService } from 'src/app/api/services/chat-message.service';
import { TokenService } from 'src/app/shared/services/token.service';
import * as moment from 'moment';
import { SimpleUserModalComponent } from 'src/app/shared/modais/simple-user-modal/simple-user-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { SocketService } from 'src/app/api/sockets/socket.service';

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"],
})
export class ChatComponent implements OnInit, OnDestroy {
  private _room = new BehaviorSubject<ChatRoomResponse>(new ChatRoomResponse());

  @Input()
  set room(value) {
    this._room.next(value);
  }

  get room() {
    return this._room.getValue();
  }

  userId: string;
  messages: ChatMessageResponse[] = [];
  loadingMessages: any[] = [
    { type: 'left' },
    { type: 'left' },
    { type: 'right' },
    { type: 'left' },
    { type: 'left' },
    { type: 'right' },
    { type: 'right' },
    { type: 'left' },
    { type: 'right' },
    { type: 'right' },
  ];

  chatTitle: string;
  chatForm: FormGroup;
  members: any[] = [];

  @ViewChild('messagesUl', null) messagesUl;

  socket: any;

  limit: number;
  skip: number;

  messageIsLoading: boolean;
  messageLoadFinish: boolean;

  private themeWrapper = document.querySelector('body');

  @HostListener('window:scroll', ['$event'])
  onScroll() {
    this.themeWrapper.style
      .setProperty(
        '--chatScroll', 'smooth'
      );
    const element = this.messagesUl.nativeElement;
    if (element.scrollTop < 150 && !this.messageIsLoading && !this.messageLoadFinish) {
      element.scrollTop = 150;
      this.getLastMessages();
    }
  }

  constructor(
    private chatMessageService: ChatMessageService,
    private formBuilder: FormBuilder,
    private tokenService: TokenService,
    private modalService: NgbModal,
    private router: Router,
    private socketService: SocketService
  ) {
  }

  ngOnInit() {
    this._room.subscribe(room => {
      if (this.socket) this.socket.unsubscribe();

      if (!this.room) return;

      this.setInitial();

      this.userId = this.tokenService.getUserId();
      this.startForm();
      this.getMessages();
      this.resetSeenRoomByUser();
      this.startSocket();
      this.setMembersData();
    });
  }

  ngOnDestroy(): void {
    this.socket.unsubscribe();
  }

  setInitial() {
    this.themeWrapper.style
      .setProperty(
        '--chatScroll', 'none'
      );
    this.messages = [];
    this.limit = 40;
    this.skip = 0;
  }

  startForm() {
    this.chatForm = this.formBuilder.group({
      message: ['', Validators.required]
    });
  }

  startSocket() {
    this.socket = this.socketService.getData(this.room._id).subscribe(res => {
      if (res.user != this.userId) {
        this.addMessage(res);
      }

      setTimeout(() => {
        this.scrollToBottom();
      }, 100)

      setTimeout(() => {
        this.resetSeenRoomByUser();
      }, 2000);

    });
  }

  addMessage(message) {
    const lastMessage = this.messages[this.messages.length - 1];
    const createAt = moment(message.createAt).format('Y-MM-DD HH:mm');

    if (lastMessage && lastMessage.createAt == createAt && lastMessage.user == message.user) {
      lastMessage.messages = [
        ...lastMessage.messages,
        ...message.messages
      ];
    } else {
      this.messages.push(message);
    }
  }

  resetSeenRoomByUser() {
    this.chatMessageService.resetSeenRoomByUser(this.room._id).subscribe(res => res);
  }

  setMembersData() {
    const { members } = this.room;
    members.map(m => {
      this.members[m.user._id] = {
        picture: m.user.picture ? m.user.picture : null,
        name: m.user.name,
      }
    });

    const member = members.find(m => m.user._id != this.userId);
    this.chatTitle = 'Conversa com ' + member.user.name;
  }

  getLastMessages() {
    this.skip += this.limit;
    this.getMessages(false);
  }

  getMessages(isNew = true) {
    this.messageIsLoading = true;
    this.chatMessageService.getByRoom(this.room._id, { limit: this.limit, skip: this.skip }).subscribe((res) => {

      if (res.length == 0) {
        this.messageLoadFinish = true;
      }

      this.messages = [
        ...res,
        ...this.messages
      ];
      this.messageIsLoading = false;

      if (!isNew) return;

      setTimeout(() => {
        this.scrollToBottom();
      }, 500);
    });
  }

  send() {
    if (this.chatForm.invalid) return;

    const { message } = this.chatForm.value;
    this.chatForm.get('message').patchValue('');

    const messageCreated = {
      room: this.room,
      user: this.userId,
      messages: [message],
      createAt: moment().format('YYYY-MM-DD HH:mm')
    };

    this.addMessage(messageCreated);

    this.chatMessageService
      .send({
        roomId: this.room._id,
        message: message,
      })
      .subscribe(() => {
      }, err => {

      });
  }

  scrollToBottom() {
    const element = this.messagesUl.nativeElement;
    element.scrollTop = element.scrollHeight;
  }

  openUserModal(userId) {
    const modalRef = this.modalService.open(SimpleUserModalComponent);
    modalRef.componentInstance.userId = userId;
  }

  goToTalks() {
    this.router.navigate(['/talk'])
  }
}
