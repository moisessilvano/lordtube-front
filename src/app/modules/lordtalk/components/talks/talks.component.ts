import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatRoomResponse } from 'src/app/api/models/response/chat-room.response';
import { ChatRoomService } from 'src/app/api/services/chat-room.service';
import { SocketService } from 'src/app/api/sockets/socket.service';
import { TokenService } from 'src/app/shared/services/token.service';

@Component({
  selector: 'app-talks',
  templateUrl: './talks.component.html',
  styleUrls: ['./talks.component.scss']
})
export class TalksComponent implements OnInit {
  rooms: ChatRoomResponse[] = [];
  userId: string;

  isLoading: boolean;
  isFinish: boolean;

  limit: number;
  skip: number;

  constructor(
    private chatRoomService: ChatRoomService,
    private tokenService: TokenService,
    private router: Router,
    private socketService: SocketService
  ) { }

  ngOnInit() {
    this.userId = this.tokenService.getUserId();
    this.limit = 15;
    this.skip = 0;
    this.getRooms();

    this.socketService.getData('talks').subscribe(res => {
      if (res.type == 'reset') {
        this.updateSeen(res.roomId);
        return;
      }

      let qntNotSeen = 0;

      const lastRoom = this.rooms.find(r => r._id == res.roomId);
      if (lastRoom && lastRoom.qntNotSeen) {
        qntNotSeen = lastRoom.qntNotSeen;
      }

      this.rooms = this.rooms.filter(r => r._id != res.roomId);

      const room = {
        ...res.room,
        lastMessage: res.lastMessage,
        qntNotSeen: qntNotSeen + 1
      };

      this.rooms = [room, ...this.rooms];
      this.rooms = this.rooms.map(r => ({
        ...r,
        members: r.members.filter(m => m.user._id != this.userId)
      }));
    });
  }

  getRooms() {
    if (this.isLoading || this.isFinish) return;
    this.isLoading = true;

    this.chatRoomService.getRoomsByUser({ limit: this.limit, skip: this.skip }).subscribe(res => {
      this.isLoading = false;

      if (res.length == 0) {
        this.isFinish = true;
        return;
      }
      this.skip += this.limit;

      this.rooms = [...this.rooms, ...res];
      this.rooms = this.rooms.map(r => ({
        ...r,
        members: r.members.filter(m => m.user._id != this.userId)
      }));
    }, err => {
      this.isLoading = false;
    })
  }

  updateSeen(roomId: string) {
    this.rooms = this.rooms.map(r => ({
      ...r,
      qntNotSeen: r._id == roomId ? 0 : r.qntNotSeen
    }))
  }

  goToChat(roomId: string) {
    this.router.navigate(['/talk/', roomId]);
  }

}
