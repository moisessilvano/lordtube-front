import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostResponse } from 'src/app/api/models/response/post.response';
import { PostService } from 'src/app/api/services/post.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  postId: string;
  post: PostResponse;

  constructor(
    private route: ActivatedRoute,
    private postService: PostService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.postId = params['id'];
      this.getPost();
    });
  }

  getPost() {
    this.postService.getPostById(this.postId).subscribe(res => this.post = res);
  }

}
