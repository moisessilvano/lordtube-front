import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { UserResponse } from 'src/app/api/models/response/user.response';
import { UserService } from 'src/app/api/services/user.service';
import { AuthService } from 'src/app/api/services/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  user: UserResponse;

  isMobile: boolean;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private titleService: Title
  ) {
    this.titleService.setTitle('LordTube');


    if (window.innerWidth > 768) {
      this.isMobile = false;
    } else {
      this.isMobile = true;
    }
  }

  ngOnInit() {
    this.getCurrentUser();


  }

  getCurrentUser() {
    this.userService.getCurrentUser().subscribe(res => this.user = res);
  }

  logout() {
    this.authService.logoutAuth();
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
