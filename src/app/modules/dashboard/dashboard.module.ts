import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ContadorPageComponent } from './pages/contador-page/contador-page.component';
import { ContatoPageComponent } from './pages/contato-page/contato-page.component';
import { UsuarioModule } from 'src/app/modules/usuario/usuario.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { HelpPageComponent } from './pages/help-page/help-page.component';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { TeamPageComponent } from './pages/team-page/team-page.component';
import { PartnersPageComponent } from './pages/partners-page/partners-page.component';
import { NotificationsPageComponent } from './pages/notifications-page/notifications-page.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { MeetPageComponent } from './pages/meet-page/meet-page.component';
import { StoryModule } from 'src/app/shared/modules/story/story.module';
import { StoriesPageComponent } from './pages/stories-page/stories-page.component';
import { PopularModule } from 'src/app/shared/modules/popular/popular.module';
import { SharePageComponent } from './pages/share-page/share-page.component';
import { PopularPageComponent } from './pages/popular-page/popular-page.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    DashboardComponent,
    HomePageComponent,
    ContadorPageComponent,
    ContatoPageComponent,
    HelpPageComponent,
    TeamPageComponent,
    PartnersPageComponent,
    NotificationsPageComponent,
    MainPageComponent,
    MeetPageComponent,
    StoriesPageComponent,
    SharePageComponent,
    PopularPageComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    UsuarioModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    PipesModule,
    StoryModule,
    PopularModule,
    InfiniteScrollModule
  ]
})
export class DashboardModule { }
