import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ContadorPageComponent } from './pages/contador-page/contador-page.component';
import { HelpPageComponent } from './pages/help-page/help-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { PartnersPageComponent } from './pages/partners-page/partners-page.component';
import { TeamPageComponent } from './pages/team-page/team-page.component';
import { NotificationsPageComponent } from './pages/notifications-page/notifications-page.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { MeetPageComponent } from './pages/meet-page/meet-page.component';
import { StoriesPageComponent } from './pages/stories-page/stories-page.component';
import { SharePageComponent } from './pages/share-page/share-page.component';
import { PopularPageComponent } from './pages/popular-page/popular-page.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      { path: 'home', component: HomePageComponent },
      { path: 'profile', loadChildren: () => import('../profile/profile.module').then(m => m.ProfileModule) },
      { path: 'settings', loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule) },
      { path: 'contador', component: ContadorPageComponent },
      { path: 'contador-de-inscritos', component: ContadorPageComponent },
      { path: 'help', component: HelpPageComponent },
      { path: 'team', component: TeamPageComponent },
      { path: 'partners', component: PartnersPageComponent },
      { path: 'notifications', component: NotificationsPageComponent },
      { path: 'main', component: MainPageComponent },
      { path: 'meet', component: MeetPageComponent },
      { path: 'stories', component: StoriesPageComponent },
      { path: 'share', component: SharePageComponent },
      { path: 'popular', component: PopularPageComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
