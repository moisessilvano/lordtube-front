import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/api/services/user.service';
import { UserResponse } from 'src/app/api/models/response/user.response';

@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.scss']
})
export class TeamPageComponent implements OnInit {

  userList: UserResponse[];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getTeam();
  }

  getTeam() {
    this.userService.getTeam().subscribe(res => {
      this.userList = res;
    })
  }

}
