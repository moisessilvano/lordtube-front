import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-contato-page',
  templateUrl: './contato-page.component.html',
  styleUrls: ['./contato-page.component.scss']
})
export class ContatoPageComponent implements OnInit {

  constructor(
    private titleService: Title,
    private meta: Meta
  ) {

    this.titleService.setTitle('Contato - LordTube');
    // tslint:disable-next-line: max-line-length
    this.meta.addTag({ name: 'keywords', content: 'contato, equipe, staff, lordtube' });
    // tslint:disable-next-line: max-line-length
    this.meta.addTag({ name: 'description', content: 'Entre em contato conosco.' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });
  }

  ngOnInit() {
  }

}
