import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UserService } from 'src/app/api/services/user.service';
import { UserResponse } from 'src/app/api/models/response/user.response';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreatePostModalComponent } from 'src/app/shared/modais/create-post-modal/create-post-modal.component';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  user: UserResponse;
  isMobile: boolean;

  constructor(
    private titleService: Title,
    private meta: Meta,
    private userService: UserService,
    private modalService: NgbModal
  ) {
    this.titleService.setTitle('LordTube - A Rede Social para Influenciadores Digitais');
    // tslint:disable-next-line: max-line-length
    this.meta.addTag({ name: 'keywords', content: 'videos, divulgacao, youtube, rede social, lordtube, lorditubi, lordetube' });
    // tslint:disable-next-line: max-line-length
    this.meta.addTag({ name: 'description', content: 'Conheça a melhor rede social para novos influenciadores digitais, faça novos amigos, novos influenciadores, divulgue seus vídeos e se torne um novo influenciador de sucesso!' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

    if (window.innerWidth > 768) {
      this.isMobile = false;
    } else {
      this.isMobile = true;
    }
  }

  ngOnInit() {
    this.getCurrentUser();
    // this.openCreateModal();
  }

  getCurrentUser() {
    this.userService.getCurrentUser().subscribe(res => this.user = res);
  }

  openCreateModal() {
    this.modalService.open(CreatePostModalComponent);
  }

}
