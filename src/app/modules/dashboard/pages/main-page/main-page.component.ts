import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/api/services/user.service';
import { UserResponse } from 'src/app/api/models/response/user.response';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  user: UserResponse;

  constructor(
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.userService.getCurrentUser().subscribe(res => this.user = res);
  }

}
