import { Component, OnInit } from '@angular/core';
import { UserPopularResponse } from 'src/app/api/models/response/user-popular.response';
import { UserPopularService } from 'src/app/api/services/user-popular.service';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';

@Component({
  selector: 'app-popular-page',
  templateUrl: './popular-page.component.html',
  styleUrls: ['./popular-page.component.scss']
})
export class PopularPageComponent implements OnInit {

  popularList: UserPopularResponse[] = [];

  limit: number = 20;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  constructor(private userPopularService: UserPopularService, private defaultToastr: DefaultToastrService) { }

  ngOnInit() {
    this.getRank();
  }

  getRank() {
    this.userPopularService.getRank({ limit: this.limit, skip: this.skip }).subscribe(res => {
      this.isLoading = false;
      console.log('res', res)

      if (res.length == 0) {
        this.isFinish = true;
        return;
      }

      this.skip += this.limit;
      this.popularList = [
        ...this.popularList,
        ...res
      ]
    }, err => {
      this.defaultToastr.error('Algo de errado aconteceu, tente novamente!');
    })
  }

}
