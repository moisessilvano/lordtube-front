import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContadorPageComponent } from './contador-page.component';

describe('ContadorPageComponent', () => {
  let component: ContadorPageComponent;
  let fixture: ComponentFixture<ContadorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContadorPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContadorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
