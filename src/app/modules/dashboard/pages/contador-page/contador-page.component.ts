import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Title, Meta } from '@angular/platform-browser';

export class Canal {
  canalUrl: string;
}

@Component({
  selector: 'app-contador-page',
  templateUrl: './contador-page.component.html',
  styleUrls: ['./contador-page.component.scss']
})
export class ContadorPageComponent implements OnInit {

  canalForm: FormGroup;
  submitted = false;

  canal = new Canal();

  @Input()
  link: string;
  urlSafe: SafeResourceUrl;

  constructor(
    private titleService: Title,
    private meta: Meta,
    private formBuilder: FormBuilder,
    public sanitizer: DomSanitizer
  ) {

    this.titleService.setTitle('Contador de Inscritos para seu Canal do YouTube em Tempo Real - LordTube');
    // tslint:disable-next-line: max-line-length
    this.meta.addTag({ name: 'keywords', content: 'contador de inscritos, inscritos, contador de inscritos youtube, inscritos em tempo real, contador de inscritos em tempo real, contagem de inscritos, contador on, contadordeinscritos, contador de subs, inscritos youtube, contador youtube, contador de inscritos online, contador de inscrito, contador de inscritos youtube em tempo real, medidor de inscritos, contador do youtube' });
    // tslint:disable-next-line: max-line-length
    this.meta.addTag({ name: 'description', content: 'Veja os inscritos de seu canal do YouTube em tempo real de uma forma simples e fácil, além de contar subs, converse e conheça novos youtubers em nosso chat online' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

  }

  ngOnInit() {
    this.formulario();
    this.atualizarContador();

  }

  formulario() {
    this.canalForm = this.formBuilder.group({
      canalUrl: ['', Validators.required]
    });
  }

  submit() {

    this.submitted = true;

    if (this.canalForm.invalid) {
      return false;
    } else {
      this.atualizarContador();
    }

  }

  atualizarContador() {
    this.canal = this.canalForm.value;
    if (this.canal.canalUrl) {
      localStorage.setItem('canalUrl', this.canal.canalUrl);
      this.link = 'https://livecounts.net/channel/' + this.canal.canalUrl;
    } else {
      if (localStorage.getItem('canalUrl')) {
        this.link = 'https://livecounts.net/channel/' + localStorage.getItem('canalUrl');
      } else {
        this.link = 'https://livecounts.net/channel/https://www.youtube.com/channel/UCVZI95CL9ugh36qAfNYl2fg';
      }
    }
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.link);
  }

}
