import { Component, OnInit } from '@angular/core';
import { UserNotificationResponse } from 'src/app/api/models/response/user-notification.response';
import { UserNotificationService } from 'src/app/api/services/user-notification.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WelcomeModalComponent } from 'src/app/shared/modais/welcome-modal/welcome-modal.component';

@Component({
  selector: 'app-notifications-page',
  templateUrl: './notifications-page.component.html',
  styleUrls: ['./notifications-page.component.scss']
})
export class NotificationsPageComponent implements OnInit {

  notifications: UserNotificationResponse[] = [];

  isLoading: boolean = false;

  constructor(
    private userNotificationService: UserNotificationService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getByUser();
  }

  getByUser() {
    this.isLoading = true;
    this.userNotificationService.getByUser().subscribe(res => {
      this.notifications = res;
      this.isLoading = false;
      this.updateSeen();

      this.notifications.map(n => {
        if (n.type == 'welcome' && !n.isSeen) {
          this.modalService.open(WelcomeModalComponent)
        }
      })
    }, err => {
      this.isLoading = false;
    })
  }

  updateSeen() {
    this.userNotificationService.updateSeen().subscribe(() => { });
  }

}
