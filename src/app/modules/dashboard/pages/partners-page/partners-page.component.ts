import { Component, OnInit } from '@angular/core';
import { PartnerService } from 'src/app/api/services/partner.service';
import { PartnerResponse } from 'src/app/api/models/response/partner.response';

@Component({
  selector: 'app-partners-page',
  templateUrl: './partners-page.component.html',
  styleUrls: ['./partners-page.component.scss']
})
export class PartnersPageComponent implements OnInit {

  partners: PartnerResponse[] = [];

  constructor(
    private partnerService: PartnerService
  ) { }

  ngOnInit() {
    this.getPartners();
  }

  getPartners() {
    this.partnerService.getAll({}).subscribe(res => {
      this.partners = res;
    })
  }

}
