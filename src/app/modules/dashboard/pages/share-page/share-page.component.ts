import { Component, OnInit } from '@angular/core';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';
import { UserPopularService } from 'src/app/api/services/user-popular.service';

@Component({
  selector: 'app-share-page',
  templateUrl: './share-page.component.html',
  styleUrls: ['./share-page.component.scss']
})
export class SharePageComponent implements OnInit {

  link: string;

  constructor(private defaultToastr: DefaultToastrService, private userPopularService: UserPopularService) { }

  ngOnInit() {
    this.getShareRef();
  }

  getShareRef() {
    this.userPopularService.getShareRef().subscribe(res => {
      if (res && res.shareRef) {
        this.link = "https://lordtube.com/s/" + res.shareRef;
      }
    }, err => {
      this.defaultToastr.error('Não foi possível obter o link de compartilhamento, tente novamente!');
    })
  }

  copyToClipboard(): void {
    let listener = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (this.link));
      e.preventDefault();
    };

    document.addEventListener('copy', listener);
    document.execCommand('copy');
    document.removeEventListener('copy', listener);
    this.defaultToastr.info('Link copiado!', null, null)
  }

}
