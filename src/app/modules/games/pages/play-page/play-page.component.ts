import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GameService } from 'src/app/api/services/game.service';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';
import { GameResponse } from 'src/app/api/models/response/game.response';
import { DomSanitizer, Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-play-page',
  templateUrl: './play-page.component.html',
  styleUrls: ['./play-page.component.scss']
})
export class PlayPageComponent implements OnInit {
  id: string;
  game: GameResponse;
  gameSrc: any;

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
    private defaultToastr: DefaultToastrService,
    public sanitizer: DomSanitizer,
    private titleService: Title,
    private meta: Meta
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getGame();
    });
  }

  getGame() {
    this.gameService.getById(this.id).subscribe(res => {
      this.game = res;
      this.gameSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.game.link);
      this.updateView();

      this.titleService.setTitle(this.game.title + ' | LordTube');
      this.meta.addTag({ name: 'keywords', content: 'jogos, online, click, jogos, browser, sem instalar, android, online, multiplayer' });
      this.meta.addTag({ name: 'description', content: 'Jogue ' + this.game.title + ' no Lordtube diretamente sem ter que instalar nada' });
      this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
      this.meta.addTag({ name: 'robots', content: 'index, follow' });

      this.meta.addTag({ property: 'og:title', content: this.game.title + ' | LordTube' });
      this.meta.addTag({ property: 'og:description', content: 'Jogue ' + this.game.title + ' no Lordtube diretamente sem ter que instalar nada' });
      this.meta.addTag({ property: 'og:image', content: this.game.image });
    }, err => {
      this.defaultToastr.error('Não foi possível obter o jogo, tente novamente mais tarde!');
      this.router.navigate(['/games']);
      return err;
    })
  }

  updateView() {
    this.gameService.updateViews(this.id).subscribe(res => { })
  }

}
