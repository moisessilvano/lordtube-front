import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/api/services/game.service';
import { GameResponse } from 'src/app/api/models/response/game.response';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  games: GameResponse[] = [];

  limit: number = 50;
  skip: number;
  isLoading: boolean;
  isFinish: boolean;

  constructor(
    private gameService: GameService,
    private defaultToastr: DefaultToastrService,
    private titleService: Title,
    private meta: Meta
  ) {
    this.titleService.setTitle('Jogos Online no LordTube');
    this.meta.addTag({ name: 'keywords', content: 'jogos, online, click, jogos, browser, sem instalar, android, online, multiplayer' });
    this.meta.addTag({ name: 'description', content: 'Os melhores jogos direto do seu smartphone ou pc sem instalar, jogue agora mesmo no LordTube' });
    this.meta.addTag({ name: 'author', content: 'Moisés Silvano' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

    this.meta.addTag({ property: 'og:title', content: 'Jogos Online no LordTube' });
    this.meta.addTag({ property: 'og:description', content: 'Os melhores jogos direto do seu smartphone ou pc sem instalar, jogue agora mesmo no LordTube' });
    this.meta.addTag({ property: 'og:image', content: 'https://lordtube.com/assets/img/logo-app.png' });
  }

  ngOnInit() {
    this.getGames();
  }

  getGames() {
    if (this.isLoading || this.isFinish) return;
    this.isLoading = true;

    return this.gameService.getAll({ limit: this.limit, skip: this.skip })
      .subscribe(res => {
        this.isLoading = false;

        if (res.length === 0) {
          this.isFinish = true;
        }

        this.games = [
          ...this.games,
          ...res
        ];

        this.skip += this.limit;
        return res;
      }, err => {
        const { error } = err;
        if (error) {
          this.defaultToastr.error(error.message);
        }
        this.isLoading = false;
        return err;
      });
  }

}
