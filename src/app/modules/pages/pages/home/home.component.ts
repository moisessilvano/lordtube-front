import { Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/api/models/response/page.response';
import { PageService } from 'src/app/api/services/page.service';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  page: PageResponse;
  path: string;

  constructor(
    private pageService: PageService,
    private defaultToastr: DefaultToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.path = params['path'];
      this.getPage();
    });
  }

  getPage() {
    this.pageService.getByUrl(this.path).subscribe(res => {
      this.page = res;

      if (!this.page) {
        this.router.navigate(['/404']);
      }

    }, err => {
      this.defaultToastr.error('Algo de errado aconteceu!');
    })
  }

}
