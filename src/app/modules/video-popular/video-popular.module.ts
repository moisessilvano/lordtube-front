import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoPopularRoutingModule } from './video-popular-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    VideoPopularRoutingModule
  ]
})
export class VideoPopularModule { }
