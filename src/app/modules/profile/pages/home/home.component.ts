import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserResponse } from 'src/app/api/models/response/user.response';
import { UserService } from 'src/app/api/services/user.service';
import { environment } from 'src/environments/environment';
import { TokenService } from 'src/app/shared/services/token.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdatePictureModalComponent } from 'src/app/shared/modais/update-picture-modal/update-picture-modal.component';
import { UpdateCoverModalComponent } from 'src/app/shared/modais/update-cover-modal/update-cover-modal.component';
import { ChatRoomService } from 'src/app/api/services/chat-room.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  imageApi = environment.apiImgUrl;

  private sub: any;
  username: string;
  user: UserResponse;
  userId: string;

  isEditableProfile: boolean;

  private themeWrapper = document.querySelector('body');

  isLoading: boolean;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private tokenService: TokenService,
    private modalService: NgbModal,
    private chatRoomService: ChatRoomService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userId = this.tokenService.getUserId();

    this.sub = this.route.params.subscribe(params => {
      this.username = params['username'];
      this.getUser();
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  getUser() {
    this.isLoading = true;

    return this.userService.getByUsername(this.username)
      .subscribe(res => {
        this.user = res;
        this.themeWrapper.style
          .setProperty(
            '--coverProfile', 'url(' + this.imageApi + this.user.cover + ') center center no-repeat'
          );

        if (this.userId == this.user._id) {
          this.isEditableProfile = true;
        } else {
          this.isEditableProfile = false;
        }

        this.isLoading = false;

        return res;
      }, err => {

        this.isLoading = false;
        return err;
      });
  }

  openUpdatePicture() {
    this.modalService.open(UpdatePictureModalComponent).result.then((result) => {
      this.getUser();
    });
  }

  openUpdateCover() {
    this.modalService.open(UpdateCoverModalComponent).result.then((result) => {
      this.getUser();
    });
  }

  getRoomBeetwenUsers() {
    this.chatRoomService.getRoomBeetwenUsers(this.user._id).subscribe(res => {
      this.router.navigate(['/talk', res._id]);
    })
  }

}
