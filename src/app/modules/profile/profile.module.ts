import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeComponent } from './pages/home/home.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { IndexComponent } from './pages/index/index.component';


@NgModule({
  declarations: [HomeComponent, IndexComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    PipesModule
  ]
})
export class ProfileModule { }
