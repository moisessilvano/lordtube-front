import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlterarSenhaComponent } from './pages/alterar-senha/alterar-senha.component';
import { AlterarDadosComponent } from './pages/alterar-dados/alterar-dados.component';
import { AlterarImagemPerfilComponent } from './pages/alterar-imagem-perfil/alterar-imagem-perfil.component';
import { AlterarImagemCapaComponent } from './pages/alterar-imagem-capa/alterar-imagem-capa.component';
import { OpcoesComponent } from './pages/opcoes/opcoes.component';

const routes: Routes = [
  { path: 'alterar-senha', component: AlterarSenhaComponent },
  { path: 'alterar-dados', component: AlterarDadosComponent },
  { path: 'alterar-imagem-perfil', component: AlterarImagemPerfilComponent },
  { path: 'alterar-imagem-capa', component: AlterarImagemCapaComponent },
  { path: 'minhas-opcoes', component: OpcoesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
