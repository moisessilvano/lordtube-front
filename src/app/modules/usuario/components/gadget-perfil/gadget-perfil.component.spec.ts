import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GadgetPerfilComponent } from './gadget-perfil.component';

describe('GadgetPerfilComponent', () => {
  let component: GadgetPerfilComponent;
  let fixture: ComponentFixture<GadgetPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GadgetPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GadgetPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
