import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../../../shared/services/token.service';
import { UsuarioService } from '../../shared/services/usuario.service';
import { Usuario } from '../../shared/models/usuario';
import { EventEmitterService } from 'src/app/shared/services/event-emitter.service';
import { ImageService } from 'src/app/shared/services/image.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-gadget-perfil',
  templateUrl: './gadget-perfil.component.html',
  styleUrls: ['./gadget-perfil.component.scss']
})
export class GadgetPerfilComponent implements OnInit {
  usuario = new Usuario();

  imageApi = environment.apiImgUrl;

  private decodeToken;
  userTokenId: string;

  userImage: any = null;
  userCoverImage: any = null;
  user: Usuario;

  private themeWrapper = document.querySelector('body');

  constructor(
    private usuarioService: UsuarioService,
    private tokenService: TokenService,
  ) {
    this.decodeToken = this.tokenService.decokeToken();
    if (this.decodeToken) {
      this.userTokenId = this.decodeToken.id;
    }

    EventEmitterService.get('updateUser').subscribe(() => {
      this.getUser();
    });
  }

  ngOnInit() {
    if (this.userTokenId) {
      this.getUser();
    }
  }

  getUser() {
    return this.usuarioService.getUser(this.userTokenId).subscribe(
      async res => {
        this.user = res;
        this.themeWrapper.style
          .setProperty(
            '--coverBackgroundPerfil', 'url(' + this.imageApi + this.user.cover + ') center center no-repeat'
          );
        return res;
      },
      err => {
        return err;
      }
    );
  }
}
