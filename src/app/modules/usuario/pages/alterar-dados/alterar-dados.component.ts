import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../shared/models/usuario';
import { UsuarioService } from '../../shared/services/usuario.service';
import { TokenService } from '../../../../shared/services/token.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EventEmitterService } from 'src/app/shared/services/event-emitter.service';

@Component({
	selector: 'app-alterar-dados',
	templateUrl: './alterar-dados.component.html',
	styleUrls: ['./alterar-dados.component.scss']
})
export class AlterarDadosComponent implements OnInit {

	private decodeToken;
	usuario = new Usuario();
	usuarioForm: FormGroup;

	constructor(
		private usuarioService: UsuarioService,
		private tokenService: TokenService,
		private toastr: ToastrService
	) {
		this.decodeToken = this.tokenService.decokeToken();
	}

	ngOnInit() {
		this.iniciarFormulario();
		this.getUser();
	}

	usuarioToDash(usuario: any) {
		EventEmitterService.get('updateUsuario').emit(usuario);
	}

	iniciarFormulario() {
		this.usuarioForm = new FormGroup({
			name: new FormControl(this.usuario.name, Validators.required),
			username: new FormControl(this.usuario.username, Validators.required),
			description: new FormControl(this.usuario.description),
		});
	}

	getUser() {
		return this.usuarioService.getUser(this.decodeToken.id)
			.subscribe(res => {
				this.usuario = res;
				this.iniciarFormulario();
				return res;
			}, err => {
				return err;
			});
	}

	onSubmit() {

		if (this.usuarioForm.invalid) {
		} else {
			let usuario = new Usuario();
			usuario = this.usuarioForm.value;
			this.usuarioService.updateData(usuario)
				.subscribe(res => {
					this.toastr.success(res.message);
					this.usuarioToDash(usuario);
					return res;
				}, err => {
					this.toastr.error(err.error.message);
					return err;
				});
		}

	}

}
