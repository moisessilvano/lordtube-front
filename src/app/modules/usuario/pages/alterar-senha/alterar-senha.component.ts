import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../shared/models/usuario';
import { UsuarioService } from '../../shared/services/usuario.service';
import { TokenService } from '../../../../shared/services/token.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-alterar-senha',
	templateUrl: './alterar-senha.component.html',
	styleUrls: ['./alterar-senha.component.scss']
})
export class AlterarSenhaComponent implements OnInit {


	private decodeToken;
	usuario = new Usuario();
	usuarioForm: FormGroup;

	constructor(
		private usuarioService: UsuarioService,
		private tokenService: TokenService,
		private toastr: ToastrService
	) {
		this.decodeToken = this.tokenService.decokeToken();
	}

	ngOnInit() {
		this.iniciarFormulario();
	}

	iniciarFormulario() {
		this.usuarioForm = new FormGroup({
			oldPassword: new FormControl('', Validators.required),
			newPassword: new FormControl('', Validators.required)
		});
	}

	onSubmit() {

		if (this.usuarioForm.invalid) {
			this.toastr.error('FORMULARIO INVALIDO!');
			return false;
		} else {
			this.usuarioService.updatePassword(this.usuarioForm.value)
				.subscribe(res => {
					this.toastr.success(res.message);
					this.iniciarFormulario();
					return res;
				}, err => {

					if (err.error.length > 0) {
						for (let i = 0; i < err.error.length; i++) {
							this.toastr.error(err.error[i].message);
						}
					} else {
						this.toastr.error(err.error.message);
					}
					return err;
				});
		}

	}

}
