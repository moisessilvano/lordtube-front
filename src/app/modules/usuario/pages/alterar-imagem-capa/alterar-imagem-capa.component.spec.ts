import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarImagemCapaComponent } from './alterar-imagem-capa.component';

describe('AlterarImagemCapaComponent', () => {
  let component: AlterarImagemCapaComponent;
  let fixture: ComponentFixture<AlterarImagemCapaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarImagemCapaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarImagemCapaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
