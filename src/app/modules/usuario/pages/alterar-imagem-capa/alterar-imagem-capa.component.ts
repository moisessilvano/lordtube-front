import { Component, NgModule, VERSION, Input, OnInit } from '@angular/core';
import { Usuario } from '../../shared/models/usuario';
import { UsuarioService } from '../../shared/services/usuario.service';
import { TokenService } from '../../../../shared/services/token.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { BrowserModule } from '@angular/platform-browser';
import { EventEmitterService } from 'src/app/shared/services/event-emitter.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-alterar-imagem-capa',
  templateUrl: './alterar-imagem-capa.component.html',
  styleUrls: ['./alterar-imagem-capa.component.scss']
})
export class AlterarImagemCapaComponent implements OnInit {

  private decodeToken;
  usuario = new Usuario();
  usuarioForm: FormGroup;

  fileName: string;
  filePreview: string;

  imageChangedEvent: any = '';
  croppedImage: any = '';

  constructor(
    private usuarioService: UsuarioService,
    private tokenService: TokenService,
    private toastr: ToastrService
  ) {
    this.decodeToken = this.tokenService.decokeToken();
  }

  ngOnInit() {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.usuarioForm = new FormGroup({
      picture: new FormControl('', Validators.required)
    });
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  onSubmit() {
    if (!this.croppedImage) {
      this.toastr.error('INSIRA UMA IMAGEM');
    } else {

      this.usuarioService.updateCover(this.croppedImage)
        .subscribe(res => {
          this.toastr.success(res.message);
          this.usuarioToDash(res);
          return res;
        }, err => {
          this.toastr.error(err.error.message);
          return err;
        });
    }
  }

  blobToFile = (theBlob: Blob, fileName: string): File => {
    const b: any = theBlob;
    b.lastModifiedDate = new Date();
    b.name = fileName;

    return <File>theBlob;
  }

  usuarioToDash(usuario: any) {
    EventEmitterService.get('updateUsuario').emit(usuario);
  }

}
