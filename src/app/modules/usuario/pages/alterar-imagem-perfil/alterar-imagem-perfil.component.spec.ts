import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarImagemPerfilComponent } from './alterar-imagem-perfil.component';

describe('AlterarImagemPerfilComponent', () => {
  let component: AlterarImagemPerfilComponent;
  let fixture: ComponentFixture<AlterarImagemPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarImagemPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarImagemPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
