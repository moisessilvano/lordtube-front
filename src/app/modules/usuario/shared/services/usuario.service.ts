import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../models/usuario';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: HttpClient
  ) { }

  getUser(userId: string): Observable<Usuario> {
    return this.http.get<any>(`${environment.apiUrl}api/users/${userId}`);
  }

  getByUsername(username: string): Observable<Usuario> {
    return this.http.get<any>(`${environment.apiUrl}users/getByUsername/${username}`);
  }

  getUsersOn(userId: string): Observable<Usuario[]> {
    return this.http.get<any>(`${environment.apiUrl}users/getUsersOn`);
  }

  updateData(usuario: Usuario) {
    return this.http.put<any>(`${environment.apiUrl}api/users`, usuario);
  }

  updatePassword(data: any) {
    return this.http.put<any>(`${environment.apiUrl}api/users/updatePassword`, data);
  }

  updatePicture(file: string) {
    return this.http.put<any>(`${environment.apiUrl}api/users/updatePicture`, { file: file });
  }

  updateCover(file: string) {
    return this.http.put<any>(`${environment.apiUrl}api/users/updateCover`, { file: file });
  }

}
