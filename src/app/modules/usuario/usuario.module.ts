import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { AlterarSenhaComponent } from './pages/alterar-senha/alterar-senha.component';
import { AlterarDadosComponent } from './pages/alterar-dados/alterar-dados.component';
import { AlterarImagemPerfilComponent } from './pages/alterar-imagem-perfil/alterar-imagem-perfil.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AlterarImagemCapaComponent } from './pages/alterar-imagem-capa/alterar-imagem-capa.component';
import { GadgetPerfilComponent } from './components/gadget-perfil/gadget-perfil.component';
import { OpcoesComponent } from './pages/opcoes/opcoes.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

@NgModule({
  declarations: [
    AlterarSenhaComponent,
    AlterarDadosComponent,
    AlterarImagemPerfilComponent, AlterarImagemCapaComponent, GadgetPerfilComponent, OpcoesComponent],
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ImageCropperModule,
    PipesModule
  ],
  exports: [
    GadgetPerfilComponent
  ]
})
export class UsuarioModule { }
