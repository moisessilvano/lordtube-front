import { Component, OnInit } from '@angular/core';
import { UserPopularService } from 'src/app/api/services/user-popular.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-shared-ref-page',
  templateUrl: './shared-ref-page.component.html',
  styleUrls: ['./shared-ref-page.component.scss']
})
export class SharedRefPageComponent implements OnInit {
  shareRef: string;

  constructor(
    private userPopularService: UserPopularService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.shareRef = params['shareRef'];
      if (this.shareRef) {
        this.add();
      }
    });
  }

  add() {
    this.userPopularService.addRef(this.shareRef).subscribe(res => {
      this.router.navigate(['/']);
    }, err => {
      this.router.navigate(['/']);
    });
  }

}
