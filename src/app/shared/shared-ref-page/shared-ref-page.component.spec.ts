import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedRefPageComponent } from './shared-ref-page.component';

describe('SharedRefPageComponent', () => {
  let component: SharedRefPageComponent;
  let fixture: ComponentFixture<SharedRefPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedRefPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedRefPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
