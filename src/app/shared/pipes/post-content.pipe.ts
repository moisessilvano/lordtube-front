import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'postContent'
})
export class PostContentPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value;
    // return value.replace(/((?:http(s)?:\/\/)?(?:www(\d)?\.)?([\w\-]+\.\w{2,})\/?((?:\?(?:[\w\-]+(?:=[\w\-]+)?)?(?:&[\w\-]+(?:=[\w\-]+)?)?))?(#(?:[^\s]+)?)?)/g, '<a target="_black" href="http$2://www$3.$4$5">$1</a>');
    // var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    // return value.replace(urlRegex, function(url) {
    //     return '<a href="' + url + '" target="_blank">' + url + '</a>';
    // });
  }

}
