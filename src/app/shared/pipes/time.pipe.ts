import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'timePipe' })

export class TimePipe implements PipeTransform {

  constructor() { }

  transform(value: string) {
    const date = moment(value);
    return date.format('HH:mm')
  }
}
