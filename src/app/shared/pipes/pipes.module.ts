import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SanitizeHtmlPipe } from './sanitize-html.pipe';
import { DatePipe } from './date.pipe';
import { PostContentPipe } from './post-content.pipe';
import { ImagePipe } from './image.pipe';
import { TimePipe } from './time.pipe';
import { DateInfoPipe } from './time-info.pipe';
import { DateInfo2Pipe } from './time-info2.pipe';

@NgModule({
  declarations: [SanitizeHtmlPipe, DatePipe, PostContentPipe, ImagePipe, TimePipe, DateInfoPipe, DateInfo2Pipe],
  imports: [
    CommonModule
  ],
  exports: [SanitizeHtmlPipe, PostContentPipe, ImagePipe, DatePipe, TimePipe, DateInfoPipe, DateInfo2Pipe]
})
export class PipesModule {

  static forRoot() {
    return {
      ngModule: PipesModule,
      providers: [],
    };
  }
}
