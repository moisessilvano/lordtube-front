import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateInfoPipe2' })

export class DateInfo2Pipe implements PipeTransform {

  constructor() { }

  transform(value: string) {
    const now = moment(new Date()); // Data de hoje
    const past = moment(value); // Outra data no passado

    if (now.format('L') == past.format('L')) {
      return 'Hoje';
    }

    if (now.add(-1, 'days').format('L') == past.format('L')) {
      return 'Ontem';
    }

    return past.format('DD/MM/YYYY')

  }

}
