import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'customDate' })

export class DatePipe implements PipeTransform {

  constructor() { }

  transform(value: string) {
    const now = moment(new Date()); // Data de hoje
    const past = moment(value); // Outra data no passado
    const duration = moment.duration(now.diff(past));

    const seconds = duration.asSeconds();
    if (seconds < 60) {
      return Math.floor(seconds) + ' s';
    }

    const minutes = duration.asMinutes();
    if (minutes < 60) {
      return Math.floor(minutes) + ' m';
    }

    const hours = duration.asHours();
    if (hours < 60) {
      return Math.floor(hours) + ' h';
    }

    const pastDays = duration.asDays();
    if (pastDays < 7) {
      return Math.floor(pastDays) + ' d';
    }

    const weeks = duration.asWeeks();
    return Math.floor(weeks) + ' sem';
  }
}
