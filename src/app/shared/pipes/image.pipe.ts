import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'imagePipe'
})
export class ImagePipe implements PipeTransform {
  imageApi = environment.apiImgUrl;

  imagem: any;

  constructor(
  ) { }

  transform(value: string): string {
    if (value && value.includes('/')) {
      return this.imageApi + null;
    }

    return this.imageApi + value;

  }

}
