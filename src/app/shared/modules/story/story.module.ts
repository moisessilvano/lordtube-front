import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DragScrollModule } from 'ngx-drag-scroll';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared.module';
import { CreateStoryComponent } from './components/create-story/create-story.component';
import { StoriesSlideComponent } from './components/stories-slide/stories-slide.component';
import { StoriesComponent } from './components/stories/stories.component';
import { RouterModule } from '@angular/router';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';

@NgModule({
  declarations: [StoriesComponent, CreateStoryComponent, StoriesSlideComponent],
  imports: [
    CommonModule,
    PipesModule,
    SharedModule,
    DragScrollModule,
    NgbModule,
    RouterModule,
    NgxUsefulSwiperModule
  ],
  exports: [
    StoriesComponent
  ],
  entryComponents: [
    CreateStoryComponent,
    StoriesSlideComponent
  ]
})
export class StoryModule { }
