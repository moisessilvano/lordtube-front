import { Component, OnInit, Input, Inject } from '@angular/core';
import { StoryResponse } from 'src/app/api/models/response/story.response';
import { StoryService } from 'src/app/api/services/story.service';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { CreateStoryComponent } from '../create-story/create-story.component';
import { StoriesSlideComponent } from '../stories-slide/stories-slide.component';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {

  stories: StoryResponse[] = [];

  limit: number = 30;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  modalOption: NgbModalOptions = {}; // not null!

  constructor(
    private storyService: StoryService,
    private defaultToastr: DefaultToastrService,
    private modalService: NgbModal,
    @Inject(DOCUMENT) private document: any
  ) { }

  elem;


  ngOnInit() {
    this.elem = document.documentElement;
    this.getStories();
  }

  createStory() {
    const modalRef = this.modalService.open(CreateStoryComponent);
    modalRef.result.then(res => {
      if (res == 'create') {
        this.skip = 0;
        this.isFinish = false;
        this.stories = [];
        this.getStories();
      }
    });
  }

  openStory(id: string) {
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    const modalRef = this.modalService.open(StoriesSlideComponent, this.modalOption);
    modalRef.componentInstance.id = id;

    this.openFullscreen();
  }

  getStories() {
    if (this.isLoading || this.isFinish) return;

    this.isLoading = true;
    this.storyService.getLast24Hours({ limit: this.limit, skip: this.skip }).subscribe(res => {
      this.isLoading = false;

      if (res.length == 0) {
        this.isFinish = true;
      }

      this.skip += this.limit;
      this.stories = [...this.stories, ...res];

    }, err => {
      this.isLoading = false;
      this.defaultToastr.error('Não foi possível carregar os stories');
    })
  }

  scrollFinish(isFinish) {
    if (isFinish) {
      this.getStories();
    }
  }

  openFullscreen() {
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
  }


}
