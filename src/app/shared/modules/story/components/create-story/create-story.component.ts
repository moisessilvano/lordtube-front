import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { StoryService } from 'src/app/api/services/story.service';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';

@Component({
  selector: 'app-create-story',
  templateUrl: './create-story.component.html',
  styleUrls: ['./create-story.component.scss']
})
export class CreateStoryComponent implements OnInit {

  base64Image: string;

  isLoading: boolean;

  constructor(
    public modal: NgbActiveModal,
    private storyService: StoryService,
    private defaultToastr: DefaultToastrService
  ) { }

  setBase64Image($event) {
    this.base64Image = $event;
  }

  closeModal() {
    this.modal.close();
  }

  ngOnInit() {
  }

  onSubmit() {
    this.isLoading = true;

    if (!this.base64Image) {
      this.defaultToastr.error('Insira uma imagem para continuar!');
      this.isLoading = false;
    } else {

      this.storyService.create({ image: this.base64Image, text: null })
        .subscribe(res => {
          this.isLoading = false;
          this.modal.close('create');
          this.defaultToastr.success('Story publicado com sucesso!');
          return res;
        }, err => {
          const { error } = err;
          if (error) {
            this.defaultToastr.error(error.message);
          }
          this.isLoading = false;
          return err;
        });
    }
  }

}
