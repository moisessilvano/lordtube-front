import { Component, OnInit, Input, Inject } from '@angular/core';
import { NgbActiveModal, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { StoryResponse } from 'src/app/api/models/response/story.response';
import { Router } from '@angular/router';
import { StoryService } from 'src/app/api/services/story.service';
import { DefaultToastrService } from 'src/app/shared/services/default-toastr.service';
import { ChatRoomService } from 'src/app/api/services/chat-room.service';
import { SwiperOptions } from 'swiper';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-stories-slide',
  templateUrl: './stories-slide.component.html',
  styleUrls: ['./stories-slide.component.scss']
})
export class StoriesSlideComponent implements OnInit {

  @Input() id: string;
  stories: StoryResponse[] = [];

  isLoading: boolean = true;

  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    autoplay: {
      delay: 3000,
    },
  };

  constructor(
    public modal: NgbActiveModal,
    config: NgbCarouselConfig,
    private router: Router,
    private storyService: StoryService,
    private defaultToastr: DefaultToastrService,
    private chatRoomService: ChatRoomService,
    @Inject(DOCUMENT) private document: any
  ) {
    config.interval = 7000;
  }

  elem;

  ngOnInit() {
    this.elem = document.documentElement;
    this.getStories();
  }

  closeModal() {
    this.modal.close();
    this.closeFullscreen();
  }

  goToProfile(username: string) {
    this.closeModal();
    this.router.navigate(['p', username])
  }

  getStories() {
    this.isLoading = true;
    this.storyService.getLast24Hours({ limit: 50, skip: 0 }).subscribe(stories => {
      this.isLoading = false;
      if (stories.length == 0) {
        this.closeModal();
        this.defaultToastr.error('Nenhuma story encontrada!');
        return;
      }

      // this.stories = stories;
      this.startByStory(stories);

    }, err => {
      this.isLoading = false;
      this.defaultToastr.error('Algo de errado aconteceu!');
    })
  }

  getRoomBeetwenUsers(userId: string) {
    this.chatRoomService.getRoomBeetwenUsers(userId).subscribe(res => {
      this.router.navigate(['/talk', res._id]);
      this.closeModal();
    }, err => {
      this.defaultToastr.error('Algo de errado aconteceu!');
    })
  }

  startByStory(stories: StoryResponse[]) {
    let index;

    stories.map((s, i) => {
      if (s._id == this.id) {
        index = i;
      }
    })

    const startStories = stories.filter((s, i) => i >= index);
    const finalStories = stories.filter((s, i) => i < index);

    this.stories = [...startStories, ...finalStories];
  }

  closeFullscreen() {
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }


}
