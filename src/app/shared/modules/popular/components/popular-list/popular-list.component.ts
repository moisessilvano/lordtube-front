import { Component, OnInit } from '@angular/core';
import { UserPopularResponse } from 'src/app/api/models/response/user-popular.response';
import { UserPopularService } from 'src/app/api/services/user-popular.service';

@Component({
  selector: 'app-popular-list',
  templateUrl: './popular-list.component.html',
  styleUrls: ['./popular-list.component.scss']
})
export class PopularListComponent implements OnInit {

  popularList: UserPopularResponse[] = [];

  limit: number = 12;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;

  constructor(private userPopularService: UserPopularService) { }


  ngOnInit() {
    this.getRank();
  }

  getRank() {
    this.userPopularService.getRank({ limit: this.limit, skip: this.skip }).subscribe(res => {
      this.isLoading = false;

      if (res.length === 0) {
        this.isFinish = true;
      }

      this.skip += this.limit;
      this.popularList = [...this.popularList, ...res];
    })
  }


}
