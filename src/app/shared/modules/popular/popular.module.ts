import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopularListComponent } from './components/popular-list/popular-list.component';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { DragScrollModule } from 'ngx-drag-scroll';

@NgModule({
  declarations: [PopularListComponent],
  imports: [
    CommonModule,
    PipesModule,
    SharedModule,
    NgbModule,
    RouterModule,
    DragScrollModule
  ],
  exports: [
    PopularListComponent
  ]
})
export class PopularModule { }
