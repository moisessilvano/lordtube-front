import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class DefaultToastrService {

    constructor(
        private toastr: ToastrService,
        private router: Router
    ) { }

    goToRoute(route: any, isInternal: boolean) {
        isInternal ?
            this.router.navigate(route) :
            window.open(route);
    }


    success(text: string) {
        this.toastr.success(text).onTap
            .pipe(take(1))
            .subscribe(() => { })
    }

    info(title: string, text: string, route: any) {
        this.toastr.info(text, title).onTap
            .pipe(take(1))
            .subscribe(() => {
                if (route) {
                    this.goToRoute(route, true)
                }
            })
    }

    warning(text: string) {
        this.toastr.warning(text).onTap
            .pipe(take(1))
            .subscribe(() => { })
    }

    error(text: string) {
        this.toastr.error(text).onTap
            .pipe(take(1))
            .subscribe(() => { })
    }

}
