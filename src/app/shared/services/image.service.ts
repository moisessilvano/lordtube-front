import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { async } from '@angular/core/testing';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  handleError: any;

  constructor(
    private http: HttpClient
  ) { }

  async getImage(imageName: string): Promise<any> {
    const result = await this.http.get(`${environment.apiUrl}images/getByName/${imageName}`, { responseType: 'blob' })
      .toPromise();
    return this.readAsDataURL(result);
  }

  async getImageById(id: string): Promise<any> {
    const result = await this.http.get(`${environment.apiUrl}images/getById/${id}`, { responseType: 'blob' })
      .toPromise();
    return this.readAsDataURL(result);
  }


  readAsDataURL(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const fr = new FileReader();
      fr.onerror = reject;
      fr.onloadend = function () {
        resolve(fr.result);
      }
      fr.readAsDataURL(file);
    });
  }
}
