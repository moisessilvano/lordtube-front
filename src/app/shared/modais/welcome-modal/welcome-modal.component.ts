import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/api/services/user.service';
import { UserResponse } from 'src/app/api/models/response/user.response';

@Component({
  selector: 'app-welcome-modal',
  templateUrl: './welcome-modal.component.html',
  styleUrls: ['./welcome-modal.component.scss']
})
export class WelcomeModalComponent implements OnInit {

  user: UserResponse;

  constructor(
    public modal: NgbActiveModal,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.userService.getUser('5e3b04ab857aee001702ee6c').subscribe(res => this.user = res);
  }

}
