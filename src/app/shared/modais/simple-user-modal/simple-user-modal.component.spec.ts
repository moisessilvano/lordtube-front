import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleUserModalComponent } from './simple-user-modal.component';

describe('SimpleUserModalComponent', () => {
  let component: SimpleUserModalComponent;
  let fixture: ComponentFixture<SimpleUserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleUserModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
