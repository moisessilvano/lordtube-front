import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserResponse } from 'src/app/api/models/response/user.response';
import { UserService } from 'src/app/api/services/user.service';
import { environment } from 'src/environments/environment';
import { ChatRoomService } from 'src/app/api/services/chat-room.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-simple-user-modal',
  templateUrl: './simple-user-modal.component.html',
  styleUrls: ['./simple-user-modal.component.scss']
})
export class SimpleUserModalComponent implements OnInit {
  @Input() userId: string;
  @Input() youWatched: boolean;
  user: UserResponse;
  imageApi = environment.apiImgUrl;

  private themeWrapper = document.querySelector('body');

  constructor(
    public modal: NgbActiveModal,
    private userService: UserService,
    private chatRoomService: ChatRoomService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    return this.userService.getById(this.userId)
      .subscribe(res => {
        this.user = res;
        this.themeWrapper.style
          .setProperty(
            '--coverPerfil', 'url(' + this.imageApi + this.user.cover + ') center center no-repeat'
          );
      });
  }

  getRoomBeetwenUsers() {
    this.chatRoomService.getRoomBeetwenUsers(this.user._id).subscribe(res => {
      this.router.navigate(['/talk', res._id]);
      this.modal.dismiss('');
    })
  }

}
