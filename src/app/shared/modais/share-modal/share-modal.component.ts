import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DefaultToastrService } from '../../services/default-toastr.service';

@Component({
  selector: 'app-share-modal',
  templateUrl: './share-modal.component.html',
  styleUrls: ['./share-modal.component.scss']
})
export class ShareModalComponent implements OnInit {

  @Input() link: string;

  constructor(
    public modal: NgbActiveModal,
    private defaultToastr: DefaultToastrService
  ) { }

  ngOnInit() {
  }

  copyToClipboard(): void {
    let listener = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (this.link));
      e.preventDefault();
    };

    document.addEventListener('copy', listener);
    document.execCommand('copy');
    document.removeEventListener('copy', listener);
    this.defaultToastr.info('Link copiado!', null, null)
  }

}
