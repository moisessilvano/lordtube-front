import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/api/services/user.service';
import { DefaultToastrService } from '../../services/default-toastr.service';

@Component({
  selector: 'app-update-picture-modal',
  templateUrl: './update-picture-modal.component.html',
  styleUrls: ['./update-picture-modal.component.scss']
})
export class UpdatePictureModalComponent implements OnInit {

  base64Image: string;

  isLoading: boolean;

  constructor(
    private userService: UserService,
    public modal: NgbActiveModal,
    private defaultToastr: DefaultToastrService
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.modal.close();
  }

  setBase64Image($event) {
    this.base64Image = $event;
  }

  onSubmit() {
    this.isLoading = true;

    if (!this.base64Image) {
      this.defaultToastr.error('Insira uma imagem para continuar!');
      this.isLoading = false;
    } else {

      this.userService.updatePicture(this.base64Image)
        .subscribe(res => {
          this.isLoading = false;
          this.modal.close();
          return res;
        }, err => {
          const { error } = err;
          if (error) {
            this.defaultToastr.error(error.message);
          }
          this.isLoading = false;
          return err;
        });
    }
  }

}
