import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ReportService } from "src/app/api/services/report.service";

@Component({
  selector: "app-report-modal",
  templateUrl: "./report-modal.component.html",
  styleUrls: ["./report-modal.component.scss"],
})
export class ReportModalComponent implements OnInit {
  reportForm: FormGroup;

  @Input() type;
  @Input() item;

  constructor(
    public modal: NgbActiveModal,
    private reportService: ReportService
  ) {}

  ngOnInit() {
    this.startForm();
  }

  startForm() {
    this.reportForm = new FormGroup({
      message: new FormControl("", [Validators.required]),
    });
  }

  onSubmit() {
    if (this.reportForm.invalid) {
      return;
    }

    const { message } = this.reportForm.value;

    this.reportService
      .send({
        item: this.item,
        type: this.type,
        message,
      })
      .subscribe((res) => {
        this.reportForm.get("message").patchValue("");
        this.modal.close();
        console.log("denuncia efetuada com sucesso");
      });
  }

  closeModal() {
    this.modal.close();
  }
}
