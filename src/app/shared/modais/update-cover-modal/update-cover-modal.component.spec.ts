import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCoverModalComponent } from './update-cover-modal.component';

describe('UpdateCoverModalComponent', () => {
  let component: UpdateCoverModalComponent;
  let fixture: ComponentFixture<UpdateCoverModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCoverModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCoverModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
