import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CustomCardComponent } from "./components/custom-card/custom-card.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { SharingComponent } from "./components/sharing/sharing.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedItemComponent } from "./components/shared-item/shared-item.component";
import { PostComponent } from "./components/post/post.component";
import { PostsComponent } from "./components/posts/posts.component";
import { PipesModule } from "./pipes/pipes.module";
import { UserGadgetComponent } from "./components/user-gadget/user-gadget.component";
import { UserMenuGadgetComponent } from "./components/user-menu-gadget/user-menu-gadget.component";
import { RouterModule } from "@angular/router";
import { AdsGadgetComponent } from "./components/ads-gadget/ads-gadget.component";
import { SocialNetworkGadgetComponent } from "./components/social-network-gadget/social-network-gadget.component";
import { UsersOnGadgetComponent } from "./components/users-on-gadget/users-on-gadget.component";
import { CommentComponent } from "./components/comment/comment.component";
import { CommentsComponent } from "./components/comments/comments.component";
import { CreateCommentComponent } from "./components/create-comment/create-comment.component";
import { SearchEmojiComponent } from "./components/search-emoji/search-emoji.component";
import { PickerModule } from "@ctrl/ngx-emoji-mart";
import { CreatePostComponent } from "./components/create-post/create-post.component";
import { SimpleUserModalComponent } from "./modais/simple-user-modal/simple-user-modal.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { ProjectMenuGadgetComponent } from "./components/project-menu-gadget/project-menu-gadget.component";
import { UpdatePictureModalComponent } from "./modais/update-picture-modal/update-picture-modal.component";
import { ImageCropperModule } from "ngx-image-cropper";
import { CustomUploadComponent } from "./components/custom-upload/custom-upload.component";
import { UpdateCoverModalComponent } from "./modais/update-cover-modal/update-cover-modal.component";
import { CaptchaComponent } from "./components/captcha/captcha.component";
import { RecaptchaModule } from "ng-recaptcha";
import { ReportModalComponent } from "./modais/report-modal/report-modal.component";
import { LastUsersComponent } from './components/last-users/last-users.component';
import { IconMessagesComponent } from './components/icon-messages/icon-messages.component';
import { IconNotificationsComponent } from './components/icon-notifications/icon-notifications.component';
import { PostsUserComponent } from './components/posts-user/posts-user.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AdsComponent } from './components/ads/ads.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { UpdateModalComponent } from './modais/update-modal/update-modal.component';
import { WelcomeModalComponent } from './modais/welcome-modal/welcome-modal.component';
import { ShareModalComponent } from './modais/share-modal/share-modal.component';
import { IconNewPostComponent } from './components/icon-new-post/icon-new-post.component';
import { CreatePostModalComponent } from './modais/create-post-modal/create-post-modal.component';
import { ContenteditableValueAccessorModule } from '@tinkoff/angular-contenteditable-accessor';
import { SharedRefPageComponent } from './shared-ref-page/shared-ref-page.component';

@NgModule({
  declarations: [
    CustomCardComponent,
    SharingComponent,
    SharedItemComponent,
    PostComponent,
    PostsComponent,
    UserGadgetComponent,
    UserMenuGadgetComponent,
    AdsGadgetComponent,
    SocialNetworkGadgetComponent,
    UsersOnGadgetComponent,
    CommentComponent,
    CommentsComponent,
    CreateCommentComponent,
    SearchEmojiComponent,
    CreatePostComponent,
    SimpleUserModalComponent,
    NavbarComponent,
    NavbarComponent,
    ProjectMenuGadgetComponent,
    UpdatePictureModalComponent,
    CustomUploadComponent,
    UpdateCoverModalComponent,
    CaptchaComponent,
    ReportModalComponent,
    LastUsersComponent,
    IconMessagesComponent,
    IconNotificationsComponent,
    PostsUserComponent,
    AdsComponent,
    NotFoundPageComponent,
    UpdateModalComponent,
    WelcomeModalComponent,
    ShareModalComponent,
    IconNewPostComponent,
    CreatePostModalComponent,
    SharedRefPageComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    RouterModule,
    PickerModule,
    ImageCropperModule,
    RecaptchaModule,
    InfiniteScrollModule,
    AngularEditorModule,
    ContenteditableValueAccessorModule
  ],
  exports: [
    CustomCardComponent,
    SharingComponent,
    PostComponent,
    PostsComponent,
    UserGadgetComponent,
    UserMenuGadgetComponent,
    CreatePostComponent,
    SimpleUserModalComponent,
    NavbarComponent,
    ProjectMenuGadgetComponent,
    CaptchaComponent,
    LastUsersComponent,
    PostsUserComponent,
    AdsComponent,
    CustomUploadComponent,
    IconNewPostComponent
  ],
  entryComponents: [
    SimpleUserModalComponent,
    UpdatePictureModalComponent,
    UpdateCoverModalComponent,
    ReportModalComponent,
    UpdateModalComponent,
    WelcomeModalComponent,
    ShareModalComponent,
    CreatePostModalComponent
  ],
})
export class SharedModule { }
