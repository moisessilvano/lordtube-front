import { Component, OnInit, Input } from '@angular/core';
import { AdventService } from 'src/app/api/services/advent.service';
import { AdventResponse } from 'src/app/api/models/response/advent.response';

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {

  ads: AdventResponse;

  @Input() isFull: boolean = true;

  constructor(
    private adventService: AdventService
  ) { }

  ngOnInit() {
    this.getAds();
  }

  getAds() {
    this.adventService.getAds().subscribe(res => {
      this.ads = res;
    })
  }

}
