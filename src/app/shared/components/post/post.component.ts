import { Component, Input, OnInit } from "@angular/core";
import { PostResponse } from "src/app/api/models/response/post.response";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ReportModalComponent } from "../../modais/report-modal/report-modal.component";
import { DomSanitizer } from '@angular/platform-browser';
import { ShareModalComponent } from '../../modais/share-modal/share-modal.component';
import { environment } from 'src/environments/environment';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.scss"],
})
export class PostComponent implements OnInit {
  @Input() post: PostResponse = new PostResponse();

  isWatchMode: boolean;

  youtubeSrc: any;

  constructor(private modalService: NgbModal, public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    if (this.post && this.post.urlData && this.post.urlData.type == 'YOUTUBE') {
      this.youtubeSrc = this.sanitizer.bypassSecurityTrustResourceUrl(
        'https://www.youtube.com/embed/' +
        this.post.urlData.videoId +
        '?wmode=transparent'
      )
    }
  }

  openReportModal(postId) {
    const modalRef = this.modalService.open(ReportModalComponent);
    modalRef.componentInstance.item = postId;
    modalRef.componentInstance.type = "post";
  }

  openShareModal(id: string) {
    const link = environment.urlProject + 'post/' + id;
    const modalRef = this.modalService.open(ShareModalComponent);
    modalRef.componentInstance.link = link;
  }

  setWatchMode() {
    this.isWatchMode = true;
  }
}
