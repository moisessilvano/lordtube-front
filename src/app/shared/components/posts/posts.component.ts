import { Component, OnInit } from '@angular/core';
import { PostResponse } from 'src/app/api/models/response/post.response';
import { PostService } from 'src/app/api/services/post.service';
import { SocketService } from 'src/app/api/sockets/socket.service';
import { DefaultToastrService } from '../../services/default-toastr.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  limit: number;
  skip: number;

  newPostList: PostResponse[] = [];

  postGroups = [];

  isLoading: boolean;
  isFinish: boolean;

  public isOnline: boolean = navigator.onLine;

  constructor(
    private postService: PostService,
    private socketService: SocketService
  ) { }

  ngOnInit() {
    this.limit = 6;
    this.skip = 0;

    this.getPosts();

    this.socketService.getData('globalPost').subscribe(res => {
      this.newPostList = [
        res,
        ...this.newPostList
      ]
    });

  }

  showNewPost() {
    if (this.postGroups && this.postGroups[0]) {
      this.postGroups[0] = [
        ...this.newPostList,
        ...this.postGroups[0]
      ]
    }
    this.newPostList = [];
  }

  getPosts() {
    if (this.isLoading || this.isFinish) return;
    this.isLoading = true;

    return this.postService.getPosts({ limit: this.limit, skip: this.skip })
      .subscribe(res => {
        this.isLoading = false;

        if (res.length === 0) {
          this.isFinish = true;
        }

        this.postGroups.push(res);

        this.skip += this.limit;
        this.saveLocalStorage();
        return res;
      }, err => {
        const postStorage = localStorage.getItem('posts');
        if (postStorage) {
          // const postList = JSON.parse(postStorage);
          // this.postGroups.push(postList);
        }

        return err;
      });
  }

  saveLocalStorage() {
    // localStorage.setItem('posts', JSON.stringify(this.postList.filter((p, i) => i < 20)));
  }

}
