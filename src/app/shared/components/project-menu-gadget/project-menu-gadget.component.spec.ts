import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectMenuGadgetComponent } from './project-menu-gadget.component';

describe('ProjectMenuGadgetComponent', () => {
  let component: ProjectMenuGadgetComponent;
  let fixture: ComponentFixture<ProjectMenuGadgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectMenuGadgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMenuGadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
