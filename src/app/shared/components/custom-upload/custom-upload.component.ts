import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-custom-upload',
  templateUrl: './custom-upload.component.html',
  styleUrls: ['./custom-upload.component.scss']
})
export class CustomUploadComponent implements OnInit {

  @Input() aspectRatio: number = 4 / 4;
  @Input() resizeToWidth: number = 500;
  @Input() maintainAspectRatio: boolean = false;

  fileName: string;
  filePreview: string;
  imageChangedEvent: any = '';
  croppedImage: any = '';

  @Output() base64Image = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.base64Image.emit(this.croppedImage);
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

}
