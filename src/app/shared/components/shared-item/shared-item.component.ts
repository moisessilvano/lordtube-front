import { Component, Input, OnInit } from '@angular/core';
import { SharingResponse } from 'src/app/api/models/response/sharing.response';

@Component({
  selector: 'app-shared-item',
  templateUrl: './shared-item.component.html',
  styleUrls: ['./shared-item.component.scss']
})
export class SharedItemComponent implements OnInit {

  @Input() item: SharingResponse;

  socialNetwork = {
    title: '',
    backgroundColor: '',
    textColor: ''
  };

  constructor() { }

  ngOnInit() {
    this.setSocialNetworkColor(this.item.type);
  }

  setSocialNetworkColor(name) {
    this.socialNetwork['title'] = this.item.type;
    switch (name) {
      case 'YOUTUBE':
        this.socialNetwork['backgroundColor'] = '#F00000';
        this.socialNetwork['textColor'] = '#FFFFFF';
        break;
      case 'TWITTER':
        this.socialNetwork['backgroundColor'] = '#1DA1F2';
        this.socialNetwork['textColor'] = '#FFFFFF';
        break;
      case 'INSTAGRAM':
        this.socialNetwork['backgroundColor'] = '#e4405f';
        this.socialNetwork['textColor'] = '#FFFFFF';
        break;
      case 'TIKTOK':
        this.socialNetwork['backgroundColor'] = '#000000';
        this.socialNetwork['textColor'] = '#FFFFFF';
        break;
      case 'FACEBOOK':
        this.socialNetwork['backgroundColor'] = '#3b5998';
        this.socialNetwork['textColor'] = '#FFFFFF';
        break;
    }
  }

}
