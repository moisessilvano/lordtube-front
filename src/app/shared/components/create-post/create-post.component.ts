import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { FileQueueObject } from 'ngx-image-uploader';
import { PostService } from 'src/app/api/services/post.service';
import { DefaultToastrService } from '../../services/default-toastr.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {
  postForm: FormGroup;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    width: '100%',
    height: 'auto',
    placeholder: 'No que você está pensando?',
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: false,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'undo',
        'redo',
        'strikeThrough',
        'subscript',
        'superscript',
        'indent',
        'outdent',
        // 'insertUnorderedList',
        // 'insertOrderedList',
        'heading',
        'fontName',
        'justifyLeft',
        'justifyCenter',
        'justifyRight',
        'justifyFull',
      ],
      [
        'backgroundColor',
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]
    ]
  };

  imageChangedEvent: any = '';
  croppedImage: any = '';

  isLoading: boolean;
  isPosted: boolean = true;

  hasText: boolean;

  @Output() postCreated = new EventEmitter();

  constructor(
    private postService: PostService,
    private defaultToastr: DefaultToastrService
  ) { }

  ngOnInit() {
    this.startForm();

    this.postForm.get('content').valueChanges.subscribe(res => {
      if (res.length > 0) {
        this.hasText = true;
      } else {
        this.hasText = false;
      }
    })
  }

  startForm() {
    this.postForm = new FormGroup({
      content: new FormControl('')
    });
  }


  getHtmlPlainText(s: string): string {
    if (!s) {
      return '';
    }
    const parser = new DOMParser();
    const html = parser.parseFromString(s, 'text/html');
    return html.body.textContent;
  }

  onUpload(file: FileQueueObject) {
    console.log(file.response);
  }

  addEmoji($event) {
    this.postForm.get('content').patchValue(this.postForm.get('content').value + $event.emoji.native);
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.isPosted = false;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  onSubmit() {
    if (this.postForm.invalid) {
      return;
    }

    const { content } = this.postForm.value;
    const newContent = content.replace(/<a href/g, '<a target="_blank" href');

    this.isLoading = true;
    this.postService.createPost({
      content: newContent,
      image: this.croppedImage
    }).subscribe(() => {
      this.postForm.get('content').patchValue('');
      this.croppedImage = null;
      this.imageChangedEvent = null;
      this.isLoading = false;
      this.isPosted = true;
      this.postCreated.emit();
    }, err => {
      const { error } = err;
      if (error) {
        this.defaultToastr.error(error.message);
      }
      this.isLoading = false;
    })
  }

  color() {
    console.log('aaa');
  }
}
