import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { environment } from "src/environments/environment";

@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.scss']
})
export class CaptchaComponent implements OnInit {
  siteKey: string = environment.siteKeyCaptcha;
  @Output() infoCaptchaToken = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  resolved(captchaResponse: string) {
    this.infoCaptchaToken.emit(captchaResponse);
  }

}
