import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconNotificationsComponent } from './icon-notifications.component';

describe('IconNotificationsComponent', () => {
  let component: IconNotificationsComponent;
  let fixture: ComponentFixture<IconNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
