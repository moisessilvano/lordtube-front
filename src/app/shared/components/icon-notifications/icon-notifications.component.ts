import { Component, OnInit } from '@angular/core';
import { SocketService } from 'src/app/api/sockets/socket.service';
import { TokenService } from '../../services/token.service';
import { UserNotificationService } from 'src/app/api/services/user-notification.service';
import { UserNotificationResponse } from 'src/app/api/models/response/user-notification.response';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WelcomeModalComponent } from '../../modais/welcome-modal/welcome-modal.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-icon-notifications',
  templateUrl: './icon-notifications.component.html',
  styleUrls: ['./icon-notifications.component.scss']
})
export class IconNotificationsComponent implements OnInit {
  userId: string;
  qntNotSeen: number = 0;
  notifications: UserNotificationResponse[] = [];


  constructor(
    private socketService: SocketService,
    private tokenService: TokenService,
    private userNotificationService: UserNotificationService,
    private titleService: Title
  ) { }

  ngOnInit() {
    this.userId = this.tokenService.getUserId();
    this.getByUser();

    this.socketService.getData('notifications').subscribe(res => {
      if (res == 'reset') {
        this.qntNotSeen = 0;
        this.titleService.setTitle('LordTube');
        return;
      }

      this.qntNotSeen++;
      if (this.qntNotSeen > 99) {
        this.qntNotSeen = 99;
      }

      if (this.qntNotSeen > 0) {
        this.titleService.setTitle('(' + this.qntNotSeen + ') LordTube');
      } else {
        this.titleService.setTitle('LordTube');
      }
    });
  }

  getByUser() {
    this.userNotificationService.getByUser().subscribe(res => {
      this.notifications = res;

      this.notifications.map(n => {
        if (!n.isSeen) {
          this.qntNotSeen++;
        }
      })

      if (this.qntNotSeen > 0) {
        this.titleService.setTitle('(' + this.qntNotSeen + ') LordTube');
      } else {
        this.titleService.setTitle('LordTube');
      }

    })
  }

}
