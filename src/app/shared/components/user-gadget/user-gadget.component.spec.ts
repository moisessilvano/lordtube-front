import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGadgetComponent } from './user-gadget.component';

describe('UserGadgetComponent', () => {
  let component: UserGadgetComponent;
  let fixture: ComponentFixture<UserGadgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGadgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
