import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserResponse } from 'src/app/api/models/response/user.response';
import { environment } from 'src/environments/environment';
import { EventEmitterService } from '../../services/event-emitter.service';
import { UpdatePictureModalComponent } from '../../modais/update-picture-modal/update-picture-modal.component';
import { UpdateCoverModalComponent } from '../../modais/update-cover-modal/update-cover-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/api/services/user.service';

@Component({
  selector: 'app-user-gadget',
  templateUrl: './user-gadget.component.html',
  styleUrls: ['./user-gadget.component.scss']
})
export class UserGadgetComponent implements OnInit {
  imageApi = environment.apiImgUrl;

  private _user = new BehaviorSubject<UserResponse>(new UserResponse());

  @Input()
  set user(value) {
    this._user.next(value);
  }

  get user() {
    return this._user.getValue();
  }

  private themeWrapper = document.querySelector('body');

  constructor(
    private modalService: NgbModal,
    private userService: UserService
  ) { }

  ngOnInit() {
    this._user.subscribe(user => {
      if (!user) return;
      this.setImageCover(user);
    })

    EventEmitterService.get('updatedUser').subscribe(res => {
      this.user = {
        ...this.user,
        ...res
      }
    });
  }

  setImageCover(user: UserResponse) {
    this.themeWrapper.style
      .setProperty(
        '--coverGadgetProfile', 'url(' + this.imageApi + user.cover + ') center center no-repeat'
      );
  }

  openUpdatePicture() {
    this.modalService.open(UpdatePictureModalComponent).result.then((result) => {
      this.getUser();
    });
  }

  openUpdateCover() {
    this.modalService.open(UpdateCoverModalComponent).result.then((result) => {
      this.getUser();
    });
  }

  getUser() {
    return this.userService.getById(this.user._id)
      .subscribe(res => {
        this.user = res;
        this.setImageCover(this.user)
        return res;
      }, err => {

        return err;
      });
  }

}
