import { Component, OnInit, Input } from '@angular/core';
import { CommentService } from 'src/app/api/services/comment.service';
import { CommentResponse } from 'src/app/api/models/response/comment.response';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  @Input() postId: string;
  commentList: CommentResponse[] = [];

  limit: number;
  skip: number;

  isLoading: boolean;
  isFinish: boolean;

  constructor(
    private commentService: CommentService
  ) {
  }

  ngOnInit() {
    this.setInitialData();
    this.getComments();
  }

  setInitialData() {
    this.commentList = [];
    this.limit = 8;
    this.skip = 0;
  }

  getComments() {
    if (this.isLoading || this.isFinish) return;
    this.isLoading = true;

    this.commentService.getCommentsByPost(this.postId, { limit: this.limit, skip: this.skip })
      .subscribe(res => {
        this.isLoading = false;

        if (res.length == 0) {
          this.isFinish = true;
          return;
        }
        this.skip += this.limit;

        this.commentList = [...this.commentList, ...res];
      }, err => {
        this.isLoading = false;
      });
  }

  commentCreated() {
    this.commentList = [];
    this.limit = 30;
    this.skip = 0;
    this.isFinish = false;
    this.getComments();
  }
}
