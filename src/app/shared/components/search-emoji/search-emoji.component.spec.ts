import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchEmojiComponent } from './search-emoji.component';

describe('SearchEmojiComponent', () => {
  let component: SearchEmojiComponent;
  let fixture: ComponentFixture<SearchEmojiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchEmojiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchEmojiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
