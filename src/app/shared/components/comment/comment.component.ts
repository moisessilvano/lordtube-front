import { Component, OnInit, Input } from "@angular/core";
import { CommentResponse } from "src/app/api/models/response/comment.response";
import { ReportModalComponent } from "../../modais/report-modal/report-modal.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TokenService } from '../../services/token.service';

@Component({
  selector: "app-comment",
  templateUrl: "./comment.component.html",
  styleUrls: ["./comment.component.scss"],
})
export class CommentComponent implements OnInit {
  @Input() comment: CommentResponse;
  userPicture: string;
  userId: string;

  constructor(private modalService: NgbModal, private tokenService: TokenService) { }

  ngOnInit() {
    this.userId = this.tokenService.getUserId();
  }

  openReportModal(commentId) {
    const modalRef = this.modalService.open(ReportModalComponent);
    modalRef.componentInstance.item = commentId;
    modalRef.componentInstance.type = "comment";
  }
}
