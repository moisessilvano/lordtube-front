import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-custom-card',
  templateUrl: './custom-card.component.html',
  styleUrls: ['./custom-card.component.scss']
})
export class CustomCardComponent implements OnInit {

  @Input() title: string;
  @Input() isModal: boolean;
  @Input() isModalFullScreen: boolean;
  @Input() hasPadding: boolean = true;
  @Input() hasMargin: boolean = true;
  @Input() hasBorderTop: boolean;
  @Input() hasHeader: boolean = true;
  @Input() hasGoBackAction: boolean;
  @Input() hasCloseButton: boolean;
  @Input() removeBackground: boolean;

  @Input() hasLink: boolean;
  @Input() click: Function;
  @Input() linkTitle: string;

  @Input() isLoading: any;

  @Input() isOnline: boolean;

  cardClass: string = '';
  headerClass: string = '';
  contentClass: string = '';

  @Output() goBackAction = new EventEmitter();
  @Output() closeButton = new EventEmitter();


  private _hasFocus = new BehaviorSubject<boolean>(false);

  @Input()
  set hasFocus(value) {
    this._hasFocus.next(value);
  }

  get hasFocus() {
    return this._hasFocus.getValue();
  }

  constructor() {
  }

  ngOnInit() {

    this._hasFocus.subscribe(() => {
      if (this.hasFocus) {
        this.cardClass += ' has-focus ';
      } else {
        this.cardClass = this.cardClass.replace(' has-focus ', '');
      }
    })

    if (this.removeBackground) {
      this.cardClass += ' remove-background ';
    }
    if (this.isModal) {
      this.cardClass += ' custom-card--modal ';
    }
    if (this.isModalFullScreen) {
      this.cardClass += ' modal-fullscreen ';
    }
    if (this.hasBorderTop) {
      this.cardClass += ' border--top ';
    }
    if (this.hasMargin) {
      this.cardClass += ' card--margin ';
    }

    if (this.hasPadding) {
      this.headerClass += ' card-header--padding ';
      this.contentClass += ' card-content--padding ';
    }

  }

  infoGoBackAction() {
    this.goBackAction.emit('clicked');
  }

  infoCloseButton() {
    this.closeButton.emit('clicked');
  }

}
