import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatRoomResponse } from 'src/app/api/models/response/chat-room.response';
import { ChatRoomService } from 'src/app/api/services/chat-room.service';
import { SocketService } from 'src/app/api/sockets/socket.service';
import { DefaultToastrService } from '../../services/default-toastr.service';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'app-icon-messages',
  templateUrl: './icon-messages.component.html',
  styleUrls: ['./icon-messages.component.scss']
})
export class IconMessagesComponent implements OnInit {
  userId: string;
  qntNotSeen: number = 0;

  rooms: ChatRoomResponse[] = [];


  constructor(
    private socketService: SocketService,
    private chatRoomService: ChatRoomService,
    private tokenService: TokenService,
    private router: Router,
    private defaultToastr: DefaultToastrService
  ) { }

  ngOnInit() {
    this.userId = this.tokenService.getUserId();
    this.getRooms();

    this.socketService.getData('talks').subscribe(res => {
      if (res.type == 'reset') {
        this.updateSeen(res.roomId);
        this.qntNotSeen = 0;
        return;
      }

      const findRoom = this.rooms.find(r => r._id == res.roomId);
      if (!findRoom) {
        const room = {
          ...res.room,
          lastMessage: res.lastMessage,
          qntNotSeen: 1
        };

        this.rooms.push(room);
      } else {
        this.rooms = this.rooms.map(r => ({
          ...r,
          qntNotSeen: r._id == res.roomId ? r.qntNotSeen + 1 : r.qntNotSeen
        }))
      }

      this.updateAllQntNotSeen();

      const currentRouter = this.router.url;
      if (!currentRouter.includes('/talk')) {

        if (res.lastMessage && res.lastMessage.name && window.innerWidth > 768) {
          this.defaultToastr.info('Mensagem de ' + res.lastMessage.name, res.lastMessage.message, ['talk', res.roomId]);
        }
      }
    });

  }

  getRooms() {
    this.chatRoomService.getRoomsByUser({ limit: 15, skip: 0 }).subscribe(res => {
      this.rooms = res;
      this.rooms = this.rooms.map(r => ({
        ...r,
        members: r.members.filter(m => m.user._id != this.userId)
      }))

      this.updateAllQntNotSeen();
    })
  }

  updateAllQntNotSeen() {
    this.qntNotSeen = 0;
    this.rooms.map(r => {
      this.qntNotSeen += r.qntNotSeen;
    })
  }

  updateSeen(roomId: string) {
    this.rooms = this.rooms.map(r => ({
      ...r,
      qntNotSeen: r._id == roomId ? 0 : r.qntNotSeen
    }))
  }

}
