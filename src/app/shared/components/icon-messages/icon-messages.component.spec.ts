import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconMessagesComponent } from './icon-messages.component';

describe('IconMessagesComponent', () => {
  let component: IconMessagesComponent;
  let fixture: ComponentFixture<IconMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
