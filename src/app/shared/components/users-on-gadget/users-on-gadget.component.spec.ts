import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersOnGadgetComponent } from './users-on-gadget.component';

describe('UsersOnGadgetComponent', () => {
  let component: UsersOnGadgetComponent;
  let fixture: ComponentFixture<UsersOnGadgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersOnGadgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersOnGadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
