import { Component, OnInit, Input } from '@angular/core';
import { UserResponse } from 'src/app/api/models/response/user.response';
import { UserService } from 'src/app/api/services/user.service';

@Component({
  selector: 'app-last-users',
  templateUrl: './last-users.component.html',
  styleUrls: ['./last-users.component.scss']
})
export class LastUsersComponent implements OnInit {

  userList: UserResponse[] = [];

  @Input() limit: number = 12;
  skip: number = 0;

  isLoading: boolean;
  isFinish: boolean;
  @Input() isInfiniteScroll: boolean = false;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    if (this.isInfiniteScroll) {
      this.limit = 30;
    }

    this.getUsers();
  }

  infiniteUsers() {
    if (this.isInfiniteScroll) {
      this.getUsers();
    }
  }

  getUsers() {
    if (this.isLoading || this.isFinish) return;
    this.isLoading = true;

    this.userService.getLastUsers({ limit: this.limit, skip: this.skip }).subscribe(res => {
      this.isLoading = false;

      if (res.length == 0) {
        this.isFinish = true;
        return;
      }

      this.skip += this.limit;

      this.userList = [...this.userList, ...res];
    })
  }
}
