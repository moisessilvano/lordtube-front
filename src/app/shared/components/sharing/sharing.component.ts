import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SharingResponse } from 'src/app/api/models/response/sharing.response';
import { SharingService } from 'src/app/api/services/sharing.service';
import { SocketService } from 'src/app/api/sockets/socket.service';
import { SimpleUserModalComponent } from '../../modais/simple-user-modal/simple-user-modal.component';
import { DefaultToastrService } from '../../services/default-toastr.service';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'app-sharing',
  templateUrl: './sharing.component.html',
  styleUrls: ['./sharing.component.scss']
})
export class SharingComponent implements OnInit {
  sharingForm: FormGroup;

  listShared: SharingResponse[] = [];
  allListShared: SharingResponse[] = [];
  allListSharedViewed: SharingResponse[] = [];
  lastItem: SharingResponse;
  myLink: string;
  time: number;
  isSubmitted: boolean;
  btnVisility: boolean;
  captchaResponse: string;
  showCaptcha: boolean;

  isLoading: boolean;

  userId: string;

  isSeenHowToShare: boolean;

  constructor(
    private sharingService: SharingService,
    private socketService: SocketService,
    private formBuilder: FormBuilder,
    private defaultToastr: DefaultToastrService,
    private modalService: NgbModal,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.userId = this.tokenService.getUserId();

    this.startForm();
    this.listItems();

    this.socketService.getData('sharing').subscribe(res => {
      this.addNewItem(res);
    })
  }

  addNewItem(item: SharingResponse) {
    this.listShared = [
      item,
      ...this.listShared.filter((ls, index) => index < 3)
    ];

    this.allListShared = [
      item,
      ...this.allListShared.filter(i => i.url != item.url).filter((ls, index) => index < 16)
    ];
  }

  get f() { return this.sharingForm.controls; }

  startForm() {
    this.sharingForm = this.formBuilder.group({
      url: ['', [Validators.required, Validators.pattern(/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi)]]
    });
  }

  onSubmit() {
    const { url } = this.sharingForm.value;

    if (!url) {
      this.defaultToastr.error('Insira uma URL para divulgar!');
      return;
    }

    this.isLoading = true;

    this.sharingService.send({
      url: url,
      recaptcha: this.captchaResponse
    }).subscribe(res => {
      this.myLink = url;
      this.setLastItem();
      this.openLastItem();
      this.isSubmitted = true;
      this.setCounter(res.time);
      this.showCaptcha = res.showCaptcha;
      this.isLoading = false;
    }, err => {
      this.isLoading = false;

      const { error } = err;
      if (!error) return;
      if (error.showCaptcha) {
        this.showCaptcha = error.showCaptcha;
      }

      if (error.message) {
        this.defaultToastr.error(error.message);
      }

      if (error.time) {
        this.isSubmitted = true;
        this.setCounter(error.time);
      }

    })

  }

  listItems() {
    this.sharingService.getAll({ limit: 30, skip: 0 }).subscribe(res => {
      this.listShared = res.filter((ls, index) => index < 4);
      this.allListShared = res;
    })
  }

  openLastItem() {
    window.open(this.lastItem.url);
  }

  setLastItem() {
    if (this.allListSharedViewed.length > 0) {
      this.allListSharedViewed.map(item => {
        this.allListShared = this.allListShared.filter(ls => ls.user != item.user);
      })
    }

    this.lastItem = this.allListShared.find(ls => ls.user != this.userId);
    this.allListSharedViewed.push(this.lastItem);

    if (this.allListSharedViewed.length > 30) {
      this.allListSharedViewed = [];
    }
  }

  setCounter(time: number) {
    this.time = time;

    console.log(this.lastItem);

    const refreshIntervalId = setInterval(() => {
      if (this.time > 0) {
        this.time--;
      } else {
        this.isSubmitted = false;
        clearInterval(refreshIntervalId);

        if (this.lastItem) {
          const modalRef = this.modalService.open(SimpleUserModalComponent)
          modalRef.componentInstance.userId = this.lastItem.user;
          modalRef.componentInstance.youWatched = true;
        }

      }

    }, 1000);
  }

  toggleBtnVisility() {
    this.btnVisility = !this.btnVisility;
  }

  toggleBtnHowToShare() {
    this.isSeenHowToShare = !this.isSeenHowToShare;
  }

  infoCaptchaToken(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }


}
