import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconNewPostComponent } from './icon-new-post.component';

describe('IconNewPostComponent', () => {
  let component: IconNewPostComponent;
  let fixture: ComponentFixture<IconNewPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconNewPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconNewPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
