import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CommentService } from 'src/app/api/services/comment.service';
@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.scss']
})
export class CreateCommentComponent implements OnInit {
  @Input() postId: string;
  @Output() commentCreated = new EventEmitter();

  commentForm: FormGroup;
  emojiListIsOpen: boolean;

  @ViewChild('content', null) content;

  i18n = {
    search: 'Search',
    emojilist: 'List of emoji',
    notfound: 'No Emoji Found',
    clear: 'Clear',
    categories: {
      search: 'Resultados de pesquisa',
      recent: 'Utilizado frequentemente',
      people: 'Pessoas',
      nature: 'Animais e natureza',
      foods: 'Comida e bebida',
      activity: 'Atividade',
      places: 'Viagens e locais',
      objects: 'Objetos',
      symbols: 'Símbolos',
      flags: 'Flags'
    },
    skintones: {
      1: 'Default Skin Tone',
      2: 'Light Skin Tone',
      3: 'Medium-Light Skin Tone',
      4: 'Medium Skin Tone',
      5: 'Medium-Dark Skin Tone',
      6: 'Dark Skin Tone',
    },
  }

  constructor(
    private commentService: CommentService
  ) { }

  ngOnInit() {
    this.startForm();
  }

  startForm() {
    this.commentForm = new FormGroup({
      content: new FormControl('', [Validators.required])
    });
  }

  get f() { return this.commentForm.controls; }

  onSubmit() {
    if (this.commentForm.invalid) {
      return;
    }

    this.commentService.createComment({
      post: this.postId,
      content: this.commentForm.value.content
    }).subscribe(() => {
      this.commentForm.get('content').patchValue('');
      this.commentCreated.emit(true);
    })
  }

  setContent(value) {
    this.commentForm.get('content').patchValue(value);
  }

  addEmoji($event) {
    this.content.nativeElement.innerHTML = this.content.nativeElement.innerHTML + ' ' + $event.emoji.native
  }

  emojiToggle() {
    this.emojiListIsOpen = !this.emojiListIsOpen;
  }

  keyPress(event: any) {
    return event.which != 13;
  }
}
