import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialNetworkGadgetComponent } from './social-network-gadget.component';

describe('SocialNetworkGadgetComponent', () => {
  let component: SocialNetworkGadgetComponent;
  let fixture: ComponentFixture<SocialNetworkGadgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialNetworkGadgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialNetworkGadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
