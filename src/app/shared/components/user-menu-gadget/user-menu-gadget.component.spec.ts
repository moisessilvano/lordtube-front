import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMenuGadgetComponent } from './user-menu-gadget.component';

describe('UserMenuGadgetComponent', () => {
  let component: UserMenuGadgetComponent;
  let fixture: ComponentFixture<UserMenuGadgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMenuGadgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMenuGadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
