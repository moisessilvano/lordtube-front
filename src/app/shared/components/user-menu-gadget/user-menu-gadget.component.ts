import { Component, OnInit, Input } from '@angular/core';
import { UserResponse } from 'src/app/api/models/response/user.response';
import { AuthService } from 'src/app/api/services/auth.service';
import { EventEmitterService } from '../../services/event-emitter.service';

@Component({
  selector: 'app-user-menu-gadget',
  templateUrl: './user-menu-gadget.component.html',
  styleUrls: ['./user-menu-gadget.component.scss']
})
export class UserMenuGadgetComponent implements OnInit {
  @Input() user: UserResponse;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {

    EventEmitterService.get('updatedUser').subscribe(res => {
      this.user = {
        ...this.user,
        ...res
      }
    });
  }

  logout() {
    this.authService.logoutAuth();
  }

}
