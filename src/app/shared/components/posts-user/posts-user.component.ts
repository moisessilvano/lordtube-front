import { Component, OnInit, Input } from '@angular/core';
import { PostResponse } from 'src/app/api/models/response/post.response';
import { PostService } from 'src/app/api/services/post.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-posts-user',
  templateUrl: './posts-user.component.html',
  styleUrls: ['./posts-user.component.scss']
})
export class PostsUserComponent implements OnInit {

  private _userId = new BehaviorSubject<string>(null);

  @Input()
  set userId(value) {
    this._userId.next(value);
  }

  get userId() {
    return this._userId.getValue();
  }

  limit: number;
  skip: number;

  postList: PostResponse[] = [];

  isLoading: boolean;
  isFinish: boolean;

  constructor(
    private postService: PostService
  ) { }

  ngOnInit() {

    this._userId.subscribe(user => {
      if (user) {
        this.limit = 6;
        this.skip = 0;;
        this.postList = [];
        this.getPostsByUser();
      }
    })
  }

  getPostsByUser() {
    if (this.isLoading || this.isFinish) return;
    this.isLoading = true;

    return this.postService.getPostsByUser(this.userId, { limit: this.limit, skip: this.skip })
      .subscribe(res => {
        if (res.length === 0) {
          this.isFinish = true;
        }

        this.postList = this.postList.concat(res);
        this.skip += this.limit;
        this.isLoading = false;
        return res;
      }, err => {
        this.isLoading = false;
        return err;
      });
  }

}
