import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdsGadgetComponent } from './ads-gadget.component';

describe('AdsGadgetComponent', () => {
  let component: AdsGadgetComponent;
  let fixture: ComponentFixture<AdsGadgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdsGadgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdsGadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
