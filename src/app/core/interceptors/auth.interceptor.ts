import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse,
    HttpHeaders
} from '@angular/common/http';
// import { AuthService } from 'src/app/modules/auth/auth.service';


@Injectable()

export class AuthInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {

        if (req.url.indexOf('/oauth') > 0) {

            const request = req.clone({
            });

            return next
                .handle(request)
                .pipe(
                    tap((ev: HttpEvent<any>) => {
                        if (ev instanceof HttpResponse) {
                        }
                    }),
                    catchError(response => {
                        if (response instanceof HttpErrorResponse) {
                            if (response.status === 401) {
                            }
                        }
                        return throwError(response);
                    })
                );

        }

        return next.handle(req);

    }
}
