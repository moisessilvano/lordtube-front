import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse,
    HttpHeaders
} from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';


@Injectable()

export class LoadingInterceptor implements HttpInterceptor {

    constructor(private spinner: NgxSpinnerService) { }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {

        this.spinner.show();

        return next
            .handle(req)
            .pipe(
                tap((ev: HttpEvent<any>) => {
                    if (ev instanceof HttpResponse) {
                        this.spinner.hide();
                    }
                }),
                catchError(response => {
                    if (response instanceof HttpErrorResponse) {
                        this.spinner.hide();
                    }
                    return throwError(response);
                })
            );

    }
}
