import { Location } from "@angular/common";
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SwPush, SwUpdate } from '@angular/service-worker';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateModalComponent } from './shared/modais/update-modal/update-modal.component';
import { SocketService } from './api/sockets/socket.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'LordTube';
  route: string;

  withoutToolbar: string[] = [
    'login',
    'register',
    'recover-password',
    'tiktok',
    'youtube',
    'games/play',
    's/',
  ];

  hasToolbar: boolean = false;
  isLogged: boolean = localStorage.getItem('token') !== null;

  constructor(
    location: Location,
    router: Router,
    update: SwUpdate,
    private modalService: NgbModal,
    push: SwPush,
    private socketService: SocketService
  ) {
    router.events.subscribe(() => {
      const route = location.path().replace('/', '');

      if (!route && !this.isLogged) {
        this.hasToolbar = false;
        return;
      }

      const check = this.withoutToolbar.find(x => route.includes(x));

      if (check) {
        this.hasToolbar = false;
      } else {
        this.hasToolbar = true;
      }
    });

    update.available.subscribe(update => {
      console.log('available')
      this.modalService.open(UpdateModalComponent);
    })
  }

  ngOnInit() {

    this.socketService.getData('authenticate').e

  }

}
