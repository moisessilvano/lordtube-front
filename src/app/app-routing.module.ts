import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from './core/guards/admin-guard';
import { AuthGuard } from './core/guards/auth-guard';
import { NotFoundPageComponent } from './shared/pages/not-found-page/not-found-page.component';
import { LoginGuard } from './core/guards/login-guard';
import { SharedRefPageComponent } from './shared/shared-ref-page/shared-ref-page.component';

const isLogged: boolean = localStorage.getItem('token') !== null;

const routes: Routes = [
  {
    path: '', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule), canActivate: [LoginGuard]
  },
  {
    path: '', loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  { path: 'acesso_aplicativo', redirectTo: 'youtube' },
  { path: 's/:shareRef', component: SharedRefPageComponent },
  { path: 'post', loadChildren: () => import('./modules/post/post.module').then(m => m.PostModule) },
  { path: 'talk', loadChildren: () => import('./modules/lordtalk/lordtalk.module').then(m => m.LordtalkModule), canActivate: [AuthGuard] },
  { path: 'administrator', loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule), canActivate: [AdminGuard] },
  { path: 'p', loadChildren: () => import('./modules/profile/profile.module').then(m => m.ProfileModule) },
  { path: 'games', loadChildren: () => import('./modules/games/games.module').then(m => m.GamesModule) },
  { path: '404', component: NotFoundPageComponent },
  { path: '', loadChildren: () => import('./modules/pages/pages.module').then(m => m.PagesModule) },
  { path: '**', component: NotFoundPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'enabled'
    }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
