// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlProject: 'https://lordtube.com/',
  apiUrl: 'http://localhost:3000/',
  apiImgUrl: 'https://api.lordtube.com/images/getById/',
  apiSocket: 'http://localhost:3000',

  // apiUrl: "https://api.lordtube.com/",
  // apiImgUrl: "https://api.lordtube.com/images/getById/",
  // apiSocket: "https://api.lordtube.com",
  siteKeyCaptcha: '6LdC-PEUAAAAABPHrEBVZP6LItid3P9e7tK9Exgg',
  version: '2.0.0'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
