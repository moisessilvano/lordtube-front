export const environment = {
  production: true,
  urlProject: "https://lordtube.com/",
  apiUrl: "https://api.lordtube.com/",
  apiImgUrl: "https://api.lordtube.com/images/getById/",
  apiSocket: "https://api.lordtube.com",
  siteKeyCaptcha: '6LdC-PEUAAAAABPHrEBVZP6LItid3P9e7tK9Exgg',
  version: '2.0.0'
};
