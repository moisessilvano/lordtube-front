# Building app
FROM node:12 as build

WORKDIR /app

COPY . .

RUN yarn install --silent
RUN yarn build

# Starting NGINX
FROM nginx

COPY ./nginx.conf /etc/nginx/nginx.conf

COPY --from=build /app/dist/angular /usr/share/nginx/html